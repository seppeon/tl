#pragma once
#include "Hz/TL/TTLCommon.hpp"

namespace Hz
{
	template <template <HZ_TTL_ARG...> class As>
	struct TTLCombineImpl
	{
		template <typename T>
		struct Check : std::false_type {};
		template <HZ_TTL_ARG ... a>
		struct Check<As<a...>> : std::true_type {};
		template <typename T>
		static constexpr bool NotAs = not Check<T>::value;

		template <typename ... a>
		struct Details;
		template <HZ_TTL_ARG ... a>
		struct Details<As<a...>>
		{
			using type = As<a...>;
		};
		template <HZ_TTL_ARG ... a, HZ_TTL_ARG ... b, typename ... c>
		struct Details<As<a...>, As<b...>, c...>
		{
			using type = typename Details<As<a ..., b ...>, c...>::type;
		};

		template <typename ... Args>
		using type = typename Details<Args...>::type;
	};
	/**
	 * @brief Combine several template template lists into a single tepmlate template list.
	 * 
	 * @ingroup Aliases
	 * @tparam As The desired type of the template template list.
	 * @tparam Args The template template lists to combine.
	 */
	template <template <HZ_TTL_ARG...> class As, typename ... Args>
	using TTLCombine = typename TTLCombineImpl<As>::template type<Args...>;
}