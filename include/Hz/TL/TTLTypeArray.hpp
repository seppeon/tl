#pragma once
#include "Hz/TL/TTLCommon.hpp"
#include "Hz/TL/TTLAppendN.hpp"

namespace Hz
{
	/**
	 * @brief Create an array of `N` copies of `T` in the template template list type specified by `As`.
	 * 
	 * @ingroup Aliases
	 * @tparam T The type to repeat.
	 * @tparam N The number of times to repeat the type.
	 * @tparam As The template template list type of the result.
	 */
	template <HZ_TTL_ARG T, size_t N, template <HZ_TTL_ARG ...> class As = TTL>
	using TTLTypeArray = TTLAppendN<As<>, T, N>;
} 