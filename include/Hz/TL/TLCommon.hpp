#pragma once
#include <cstddef>
#include <type_traits>
#include <utility>

namespace Hz
{
	/**
	 * @brief The default type list.
	 * 
	 * @tparam Args... The types in the type list.
	 */
	template<typename... Args>
	struct TL{};
	/**
	 * @brief An empty type list.
	 * 
	 * @ingroup Aliases
	 */
	using EmptyList = TL<>;
	/**
	 * @brief A type that can be used to represent a single "Type", without needing a definition of the type.
	 * 
	 * @tparam T The type to represent.
	 */
	template<typename T>
	struct Type { using type = T; };

	namespace Impl
	{
		template <template<typename...> class As, typename Front, typename... Args>
		auto TLFront(Type<As<Front, Args...>>)->Type<Front>;

		template <template<typename...> class As, typename... Args>
		auto TLBack(Type<As<Args...>>)->decltype((Type<Args>{},...));

		template <template<typename...> class As, typename... Args>
		constexpr std::size_t TLLength(Type<As<Args...>>)
		{
			return sizeof...(Args);
		}

		template<typename As, typename... Args>
		struct TLEmpty;
		template<template<typename...> class As, typename... Args>
		struct TLEmpty<Type<As<Args...>>>
		{
			using type = As<>;
		};


		template<typename As, typename... Args>
		struct TLPopFront;
		template <template<typename...> class As, typename Front, typename... Args>
		struct TLPopFront<Type<As<Front, Args...>>>
		{
			using type = As<Args...>;
		};
	}
	/**
	 * @brief Get the first type in a type list.
	 * 
	 * @ingroup Aliases 
	 *
	 * @tparam List The type list.
	 */
	template<typename List>
	using TLFront = typename decltype(Impl::TLFront(Type<List>{}))::type;
	/**
	 * @brief Get the last type in a type list.
	 * 
	 * @ingroup Aliases 
	 *
	 * @tparam List The type list.
	 */
	template<typename List>
	using TLBack = typename decltype(Impl::TLBack(Type<List>{}))::type;
	/**
	 * @brief Get the "length" of the type list (ie. The number of types in the list).
	 * 
	 * @ingroup Variables
	 *
	 * @tparam List The type list.
	 */
	template <typename List>
	inline constexpr size_t TLLength = Impl::TLLength(Type<List>{});
	/**
	 * @brief Get an empty type list with the same list type as the type `List`.
	 * 
	 * @ingroup Aliases 
	 *
	 * @tparam List The type of the given list.
	 */
	template<typename List>
	using TLEmpty = typename Impl::TLEmpty<Type<List>>::type;

	namespace Impl
	{
		template <typename Lst, typename Output = TLEmpty<Lst>>
		struct PopBack;
		template <template <typename...> class As, typename Front, typename ... Outs>
		struct PopBack<As<Front>, As<Outs...>> { using type = As<Outs...>; };
		template <template <typename...> class As, typename Front, typename ... Items, typename ... Outs>
		struct PopBack<As<Front, Items...>, As<Outs...>> { using type = typename PopBack<As<Items...>, As<Outs..., Front>>::type; };
	}
	/**
	 * @brief Remove the first type from some type list.
	 * 
	 * @ingroup Aliases 
	 *
	 * @tparam List The list to remove the first type from.
	 */
	template<typename List>
	using TLPopFront = typename Impl::TLPopFront<Type<List>>::type;
	/**
	 * @brief Remove the last type from some type list.
	 * 
	 * @ingroup Aliases 
	 *
	 * @tparam List The list to remove the last type from.
	 */
	template<typename List>
	using TLPopBack = typename Impl::PopBack<List>::type;
}