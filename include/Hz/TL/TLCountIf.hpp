#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	template <bool count_v, bool next_v = true, size_t amount_v = 1>
	struct CountIfPredicate
	{
		static constexpr bool next = next_v;
		static constexpr bool count = count_v;
		static constexpr size_t amount = amount_v;
	};

	namespace Impl
	{
		template<typename type, template <typename, typename> class kernal = std::is_same>
		struct CountTypesPredicate
		{
			template <typename current>
			using process = CountIfPredicate<kernal<type, current>::value>;
		};
		template<typename type, template <typename, typename> class kernal = std::is_same>
		struct FirstMatchPredicate
		{
			template <typename current>
			static constexpr bool match = kernal<type, current>::value;

			template <typename current>
			using process = CountIfPredicate<!match<current>, !match<current>>;
		};
	};

	template <typename types, typename predicate>
	class CountIf
	{
	private:
		template <typename d, typename ... items >
		struct Looper;
		template <template<typename...> class As, typename d>
		struct Looper<d, As<>>
		{
			static constexpr size_t count = 0;
		};
		template <template<typename...> class As, typename d, typename item >
		struct Looper<d, As< item > >
		{
			using result = typename predicate::template process<item >;
			static constexpr size_t count = (result::count) ? result::amount : size_t(0);
		};
		template <template<typename...> class As, typename d, typename ... items >
		struct Looper<d, As< items ... > >
		{
			using list = As< items ... >;
			using result = typename predicate::template process<TLFront<list>>;
			using rest_r = TLPopFront<list>;

			static constexpr size_t next_count = (result::next) ? Looper<void, rest_r>::count : size_t(0);
			static constexpr size_t count = ((result::count) ? result::amount : 0) + next_count;
		};
	public:
		static constexpr size_t count = Looper<void, types>::count;
	};
	/**
	 * @brief Get the number of times `T` matches the types in `List` according to the `Predicate`.
	 *
	 * @ingroup Variables
	 *
	 * @tparam List The list to traverse.
	 * @tparam T The type to compare with types in the list.
	 * @tparam Predicate The predicate to use for comparisons.
	 */
	template <typename List, typename T, template <typename, typename> class Predicate = std::is_same>
	inline constexpr auto TLMatchCount = CountIf<List, Impl::CountTypesPredicate<T, Predicate>>::count;
	/**
	 * @brief Get the index of the first match with a type `T` according to the `Predicate`.
	 *
	 * @ingroup Variables
	 * 
	 * @tparam List The list to traverse.
	 * @tparam T The type to compare with types in the list.
	 * @tparam Predicate The predicate to use for comparisons.
	 */
	template <typename List, typename T, template <typename, typename> class Predicate = std::is_same>
	inline constexpr auto TLFirstMatch = CountIf<List, Impl::FirstMatchPredicate<T, Predicate>>::count;
};