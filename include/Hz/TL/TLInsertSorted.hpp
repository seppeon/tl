#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLForEach.hpp"
#include "Hz/TL/TLCountIf.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Types, typename inserted_item, typename ranker>
		struct SortInsetPredicate;
		template <template<typename...> class As, typename ... Args, typename inserted_item, typename ranker>
		struct SortInsetPredicate<As<Args...>, inserted_item, ranker>
		{
			template <typename type>
			static constexpr int const	get_rank	= ranker::template process<type>::value;
			static constexpr int const	item_rank	= get_rank<inserted_item>;

			template <typename type>
			static constexpr bool const	eol = (item_rank <= get_rank<type>);

			template <typename type>
			using normal_code = typename std::conditional<eol<type>, As<inserted_item, type>, type>::type;

			template <typename type>
			using process = TLForEachPredicate<normal_code<type>, !eol<type>, inserted_item>;
		};
	};
	/**
	 * @brief Insert a type into a type list, while maintaining some sorted order.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The type list.
	 * @tparam T The type to insert into the type list.
	 * @tparam Ranker The "Ranker" that is used to rank each type.
	 */
	template <typename List, typename T, typename Ranker>
	using TLInsertSorted = TLForEach<List, Impl::SortInsetPredicate<List, T, Ranker>>;
	/**
	 * @brief An example "ranker", which calculates rank of a type based on the index which it appears in a given type list.
	 *
	 * For example:
	 * @code
	 * TypeRankPredicate<TL<int, char, bool>>::template process<char>
	 * @endcode
	 * would return a rank of 1, as char is at index 1 in the given type list.
	 *
	 * @tparam List The type list.
	 */
	template <typename List>
	struct TypeRankPredicate
	{
		template <typename type>
		struct process
		{
			static constexpr int const value = Hz::TLFirstMatch<List, type>;
		};
	};
}