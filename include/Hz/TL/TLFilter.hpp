#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename F, typename List, typename Result = Hz::TLEmpty<List>>
		struct Filter;
		template <typename F, template<typename...> class As, typename ... Done>
		struct Filter<F, As<>, As<Done...>>
		{
			using type = As<Done...>;
		};
		template <typename F, template<typename...> class As, typename Front, typename ... Rest, typename ... Done>
		struct Filter<F, As<Front, Rest...>, As<Done...>>
		{
			template <typename ... Extra>
			using filter = typename Filter<F, As<Rest...>, As<Done..., Extra...>>::type;

			template <typename T, bool>
			struct Eval;
			template <typename T>
			struct Eval<T, true>{ using type = filter<Front>; };
			template <typename T>
			struct Eval<T, false>{ using type = filter<>; };

			using type = typename Eval<void, F::template Test<Front>>::type;
		};
	}
	/**
	 * @brief Filter a type list with a filter object.
	 * 
	 * A filter object is a type with a `static constexpr bool` member variable `Test`.
	 * 
	 * @ingroup Aliases
	 * @tparam List The list to filter.
	 * @tparam F The filtering object (for example: see @ref `TLSizeAtLeast` as an example of a valid filter).
	 */
	template <typename List, typename F>
	using TLFilter = typename Impl::Filter<F, List>::type;
	/**
	 * @brief Get the first type that was kept in the type list after filtering.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to filter.
	 * @tparam F The filtering object.
	 */
	template <typename List, typename F>
	using TLFirstFiltered = TLFront<TLFilter<List, F>>;
	/**
	 * @brief Get the last type that was kept in the type list after filtering.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to filter.
	 * @tparam F The filtering object.
	 */
	template <typename List, typename F>
	using TLLastFiltered = TLBack<TLFilter<List, F>>;
	/**
	 * @brief A filter object to compare the size of a type with `N`.
	 * 
	 * @tparam N The minimum size of the type to accept.
	 */
	template <size_t N>
	struct TLSizeAtLeast
	{
		template <typename T>
		static constexpr bool Test = (sizeof(T) >= N);
	};
	/**
	 * @brief A filter object to compare the size of a type with `N`.
	 * 
	 * @tparam N The maximum size of the type to accept.
	 */
	template <size_t N>
	struct TLSizeAtMost
	{
		template <typename T>
		static constexpr bool Test = (sizeof(T) <= N);
	};
	/**
	 * @brief A filter object to compare the size of a type with `N`.
	 * 
	 * @tparam Min The minimum size of the type to accept.
	 * @tparam Max The maximum size of the type to accept.
	 */
	template <size_t Min, size_t Max>
	struct TLSizeBetween
	{
		template <typename T>
		static constexpr bool Test = (Min <= sizeof(T)) and (sizeof(T) <= Max);
	};
}