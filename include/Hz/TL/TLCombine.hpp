#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <template <typename...> class As>
		struct TLCombine
		{
			template <typename T>
			struct Check : std::false_type {};
			template <typename ... a>
			struct Check<As<a...>> : std::true_type {};
			template <typename T>
			static constexpr bool NotAs = not Check<T>::value;

			template <typename ... a>
			struct Details
			{
				using type = As<a...>;
			};
			template <typename ... a>
			struct Details<As<a...>>
			{
				using type = As<a...>;
			};
			template <typename ... a, typename b, typename ... c> requires (NotAs<b>)
			struct Details<As<a...>, b, c...>
			{
				using type = typename Details<As<a..., b>, c...>::type;
			};
			template <typename a, typename ... b, typename ... c> requires (NotAs<a>)
			struct Details<a, As<b...>, c...>
			{
				using type = typename Details<As<a, b...>, c...>::type;
			};
			template <typename ... a, typename ... b, typename ... c>
			struct Details<As<a...>, As<b...>, c...>
			{
				using type = typename Details<As<a ..., b ...>, c...>::type;
			};

			template <typename ... Args>
			using type = typename Details<Args...>::type;
		};
	}
	/**
	 * @brief Combine two or more type lists or values.
	 *
	 * @ingroup Aliases 
	 *
	 * @tparam As The type of the type list (ie. `Hz::TL` or `std::tuple` etc...)
	 * @tparam Args The arguments to combine.
	 */
	template <template <typename...> class As, typename ... Args>
	using TLCombine = typename Impl::TLCombine<As>::template type<Args...>;
}