#pragma once
#include "Hz/TL/TTLIsSame.hpp"

namespace Hz
{
	namespace Impl
	{
		template<typename types, HZ_TTL_ARG T>
		struct TTLContains;
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG ... Args, HZ_TTL_ARG T>
		struct TTLContains<As<Args...>, T>
		{
			static constexpr bool value = (ttl_is_same_v<Args, T> or...);
		};
	}
	/**
	 * @brief Check if a template template list contains a given template template parameter.
	 * 
	 * @ingroup Variables
	 * @tparam List The template template list.
	 * @tparam T The template template parameter.
	 */
	template<typename List, HZ_TTL_ARG T>
	inline constexpr bool ttl_contains_v = Impl::TTLContains<List, T>::value;
}