#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <template <typename ...> class ... Args>
		struct TranslateTTL{};

		template <template <typename...> class C, typename ... Args>
		auto attempt_apply(TranslateTTL<C>, Hz::TL<Args...>, int) -> C<Args...>;
		template <template <typename ...> class C, typename ... Args>
		auto attempt_apply(TranslateTTL<C>, Hz::TL<Args...>, ...) -> C<Hz::TLFront<Hz::TL<Args...>>>;

		template <typename A, template <typename...> typename To>
		struct TLTranslate;
		template <template <typename...> class C, typename... Froms, template <typename...> typename To>
		struct TLTranslate<C<Froms...>, To>
		{
			using type = decltype(attempt_apply(TranslateTTL<To>{}, Hz::TL<Froms...>{}, 1));
		};
	}
	/**
	 * @brief Translate a type list so that the types within that list, are placed into another type list type.
	 *
	 * For example:
	 * @code
	 * TLTranslate<TL<int, char, bool>, std::tuple>
	 * @endcode 
	 * would produce a `std::tuple<int, char, bool>`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam From The source list.
	 * @tparam To The type of the destination list.
	 */
	template <typename From, template <typename...> typename To>
	using TLTranslate = typename Impl::TLTranslate<From, To>::type;
}