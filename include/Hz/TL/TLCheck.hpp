#pragma once

namespace Hz
{
	namespace Impl
	{
		template <typename List, template <typename T> class Checker>
		struct TLCheck;
		template <template <typename ...> class As, typename ... Ts, template <typename T> class Checker>
		struct TLCheck<As<Ts...>, Checker>
		{
			static constexpr bool value = (Checker<Ts>::value and ...);
		};
	}
	/**
	 * @brief Apply some checker to a list of types.
	 *
	 * The `Checker` must have the a `static constexpr bool value` data member.
	 * The checker is applied to every element of the list, only if all checks pass does this concept pass.
	 * 
	 * @ingroup Concepts
	 *
	 * @tparam List The list to check.
	 * @tparam Checker The checker.
	 */
	template <typename List, template <typename T> class Checker>
	concept TLCheck = Impl::TLCheck<List, Checker>::value;
}