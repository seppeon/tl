#pragma once
#include "Hz/TL/TLRemoveFirst.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename types, typename match, size_t count>
		struct RemoveN
		{
			using temp = TLRemoveFirst<types, match>;
			using type = typename RemoveN<temp, match, count - 1>::type;
		};
		template <typename types, typename match>
		struct RemoveN<types, match, 0>
		{
			using type = types;
		};
	}
	/**
	 * @brief Remove `N` matches of a type from a type list.
	 *
	 * @ingroup Aliases 
	 * 
	 * @tparam List The list to remove the types from.
	 * @tparam Match The match to remove from the type list.
	 * @tparam N The number of matches (at most) to remove from the type list.
	 */
	template <typename List, typename Match, size_t N>
	using TLRemoveN = typename Impl::RemoveN<List, Match, N>::type;
};