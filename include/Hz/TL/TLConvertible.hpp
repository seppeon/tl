#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Lhs, typename Rhs>
		struct TLConvertible;
		template <template <typename...> class As, typename ... Lhs, typename ... Rhs>
		struct TLConvertible<As<Lhs...>, As<Rhs...>>
		{
			static constexpr bool value = false;
		};
		template <template <typename...> class As, typename ... Lhs, typename ... Rhs>
			requires(sizeof...(Lhs) == sizeof...(Rhs))
		struct TLConvertible<As<Lhs...>, As<Rhs...>>
		{
			static constexpr bool value = ( std::convertible_to<Lhs, Rhs> and ... );
		};
	}
	/**
	 * @brief Check if the elements of two type lists are element-wise convertible.
	 *
	 * @ingroup Concepts
	 *
	 * @tparam Lhs The list to convert from.
	 * @tparam Rhs The list to convert to.
	 */
	template <typename Lhs, typename Rhs>
	concept TLConvertible = Impl::TLConvertible<Lhs, Rhs>::value;
}