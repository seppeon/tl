#pragma once
#include <cstddef>

namespace Hz
{
	namespace Impl
	{
		template <typename List, typename T, size_t N>
		struct AppendN;
		template <template<typename...> class As, typename T>
		struct AppendN<As<>, T, 0>
		{
			using type = As<>;
		};
		template <template<typename...> class As, typename T, size_t N>
		struct AppendN<As<>, T, N>
		{
			using type = typename AppendN<As<T>, T, N - 1>::type;
		};
		template <template<typename...> class As, typename T, typename ... Types>
		struct AppendN<As<Types...>, T, 0>
		{
			using type = As<Types...>;
		};
		template <template<typename...> class As, typename T, typename ... Types, size_t N>
		struct AppendN<As<Types...>, T, N>
		{
			using type = typename AppendN<As<Types..., T>, T, N - 1>::type;
		};
	}
	/**
	 * @brief Append `N` `T`s to the type list defined by `List`.
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam List The list to append items to.
	 * @tparam T The type to append to the list.
	 * @tparam N The number of times to append the type.
	 */
	template <typename List, typename T, size_t N>
	using TLAppendN = typename Impl::AppendN<List, T, N>::type;
}