#pragma once
#include "Hz/TL/TLForEach.hpp"
#include "Hz/TL/TLCountIf.hpp"

namespace Hz
{
	/**
	 * @brief Check if all types in a type list are identical.
	 *
	 * @ingroup Variables 
	 *
	 * @tparam types The types to check.
	 * @tparam kernal The kernal that will be used to check the types are the "same", the default is `std::is_same`.
	 */
	template <typename types, template <typename, typename> class kernal = std::is_same>
	inline constexpr bool TLAllSame = ( TLMatchCount<types, TLFront<types>, kernal> == TLLength<types> );
}