#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Lst, size_t I, typename Buf = Hz::TLEmpty<Lst>>
		struct RemoveAt;

		template <template<typename...> class As, size_t I, typename ... Tail>
		struct RemoveAt<As<>, I, As<Tail...>>
		{
			using type = As<Tail...>;
		};

		template <template<typename...> class As, typename First, typename ... Args, typename ... Tail>
		struct RemoveAt<As<First, Args...>, 0, As<Tail...>>
		{
			using type = As<Tail..., Args...>;
		};

		template <template<typename...> class As, typename First, typename ... Args, size_t I, typename ... Tail>
			requires (I != 0)
		struct RemoveAt<As<First, Args...>, I, As<Tail...>>
		{
			using type = typename RemoveAt<As<Args...>, (I - 1), As<Tail..., First>>::type;
		};
	}
	/**
	 * @brief Remove the type at index `I` from the type list `List`
	 * 
	 * @ingroup Aliases
	 * 
	 * @tparam List The type list.
	 * @tparam I The index.
	 */
	template <typename List, size_t I>
	using TLRemoveAt = typename Impl::RemoveAt<List, I>::type;
};