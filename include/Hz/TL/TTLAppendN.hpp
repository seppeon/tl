#pragma once
#include "Hz/TL/TTLCommon.hpp"
#include <cstddef>

namespace Hz
{
	namespace Impl
	{
		template <typename List, HZ_TTL_ARG T, std::size_t N>
		struct TTLAppendN;
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG T>
		struct TTLAppendN<As<>, T, 0>
		{
			using type = As<>;
		};
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG T, std::size_t N>
		struct TTLAppendN<As<>, T, N>
		{
			using type = typename TTLAppendN<As<T>, T, N - 1>::type;
		};
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG T, HZ_TTL_ARG ... Types>
		struct TTLAppendN<As<Types...>, T, 0>
		{
			using type = As<Types...>;
		};
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG T, HZ_TTL_ARG ... Types, std::size_t N>
		struct TTLAppendN<As<Types...>, T, N>
		{
			using type = typename TTLAppendN<As<Types..., T>, T, N - 1>::type;
		};
	}
	/**
	 * @brief Append `N` elements of type `T` to list `List`.
	 * 
	 * @ingroup Aliases
	 * @tparam List The template template list to append to.
	 * @tparam T The type to append.
	 * @tparam N The number of times to append `T`.
	 */
	template <typename List, HZ_TTL_ARG T, std::size_t N>
	using TTLAppendN = typename Impl::TTLAppendN<List, T, N>::type;
}