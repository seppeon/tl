#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename, typename>
		struct CombineSeq {};

		template <std::size_t ... A, std::size_t ... B>
		struct CombineSeq<std::index_sequence<A...>, std::index_sequence<B...>>
		{
			using type = std::index_sequence<A..., B...>;
		};

		template <typename preducate, std::size_t Index, typename Indexes, typename ... >
		struct MatchIndexes;
		template <template<typename...> class As, typename predicate, std::size_t Index, std::size_t ... Indexes, typename head>
		struct MatchIndexes<predicate, Index, std::index_sequence<Indexes...>, As<head>>
		{
			using type = std::conditional_t<predicate::template match<head>, std::index_sequence<Indexes..., Index>, std::index_sequence<Indexes...>>;
		};
		template <template<typename...> class As, typename predicate, std::size_t Index, std::size_t ... Indexes, typename head, typename ... tail>
		struct MatchIndexes<predicate, Index, std::index_sequence<Indexes...>, As<head, tail ... >>
		{
			using rest = As< tail ...>;
			using more = typename MatchIndexes<predicate, Index + 1u, std::index_sequence<Indexes...>, rest>::type;
			using type = std::conditional_t<predicate::template match<head>, typename CombineSeq<std::index_sequence<Index>, more>::type, more>;
		};
	}
	/**
	 * @brief A predicate that can be used to check if types are the same.
	 * 
	 * @tparam MatchType The type to match match on.
	 */
	template <typename MatchType>
	struct SameTypePredicate
	{
		template <typename T>
		static bool constexpr match = std::is_same_v<MatchType, T>;
	};
	/**
	 * @brief A predicate that can be used to check if types are different.
	 * 
	 * @tparam MatchType The type to fail a match match on.
	 */
	template <typename MatchType>
	struct NotSameTypePredicate
	{
		template <typename T>
		static bool constexpr match = !std::is_same_v<MatchType, T>;
	};
	/**
	 * @brief Get the indexes of matches (as defined by a `predicate`).
	 *
	 * The type produced by this alias is a `std::index_sequence<...>`.
	 *
	 * @ingroup Aliases
	 *
	 * @tparam list The list to look for matches.
	 * @tparam predicate The predicate to apply to each element in the list.
	 */
	template <typename list, typename predicate>
	using TLMatchIndexes = typename Impl::MatchIndexes<predicate, 0, std::index_sequence<>, list>::type;
}