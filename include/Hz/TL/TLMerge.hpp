#pragma once
#include "Hz/TL/TLRemoveDuplicates.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename ListA, typename ... Lists>
		struct Merge;
		template <template<typename...> class As, typename ... Args>
		struct Merge<As<Args...>>
		{
			using type = As<Args...>;
		};
		template <template<typename...> class As, typename ... ArgsA, typename ... ArgsB>
		struct Merge<As<ArgsA...>, As<ArgsB...>>
		{
			using type = Hz::TLRemoveDuplicates<As<ArgsA..., ArgsB...>>;
		};
		template <template<typename...> class As, typename ... ArgsA, typename ... ArgsB, typename ... Lists>
		struct Merge<As<ArgsA...>, As<ArgsB...>, Lists...>
		{
			using primary_t = Hz::TLRemoveDuplicates<As<ArgsA..., ArgsB...>>;
			using type = typename Merge<primary_t, Lists...>::type;
		};
	}
	/**
	 * @brief Merge the given lists, removing duplicate types.
	 *
	 * @ingroup Aliases 
	 *
	 * @tparam Args... The type lists to merge.
	 */
	template <typename ... Args>
	using TLMerge = typename Impl::Merge<Args...>::type;
}