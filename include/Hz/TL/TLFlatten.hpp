#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
    namespace Impl
    {
        template <template<typename...> class As>
        struct Flatten
        {
            template <typename T>
            struct Check : std::false_type {};
            template <typename ... a>
            struct Check<As<a...>> : std::true_type {};
            template <typename T>
            static constexpr bool NotAs = not Check<T>::value;

            template <typename T, typename U>
            struct Details;
            template <typename... Ts, typename... Heads, typename... Tail>
            struct Details<As<Ts...>, As<As<Heads...>, Tail...>> 
            {
                using type = typename Details<As<Ts...>, As<Heads..., Tail...>>::type;
            };
            template <typename... Ts, typename Head, typename... Tail>
                requires (NotAs<Head>)
            struct Details<As<Ts...>, As<Head, Tail...>>
            {
                using type = typename Details<As<Ts..., Head>, As<Tail...>>::type;
            };
            template <typename... Ts>
            struct Details<As<Ts...>, As<>>
            {
                using type = As<Ts...>;
            };

            template <typename T>
            using type = typename Details<As<>, T>::type;
        };
    }
    /**
     * @brief Flatten a type list, removing nestings from type lists.
     *
     * Example: `TL<int, TL<short, bool>>` will be flattened into `TL<int, short, bool>`.
     *
     * @ingroup Aliases 
     *
     * @tparam As The type of the type list that will be flattened.
     * @tparam List The type of the list to flatten.
     */
    template <template<typename...> class As, typename List>
    using TLFlatten = typename Impl::Flatten<As>::template type<List>;
}