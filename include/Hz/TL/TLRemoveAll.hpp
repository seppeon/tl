#pragma once
#include "Hz/TL/TLRemoveFirst.hpp"

namespace Hz
{
	/**
	 * @brief Remove all matches from a type list.
	 *
	 * @ingroup Aliases 
	 * 
	 * @tparam List The type list to remove elemnets from.
	 * @tparam Match The type to remove from the type list.
	 */
	template <typename List, typename Match>
	using TLRemoveAll = TLForEach<List, Impl::RemovePredicate<TLEmpty<List>, Match>>;
};