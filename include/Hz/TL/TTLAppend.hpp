#pragma once
#include "Hz/TL/TTLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Obj, typename List>
		struct TTLAppend;

		template <template <HZ_TTL_ARG ...> class Obj, HZ_TTL_ARG ... As, HZ_TTL_ARG ... Args>
		struct TTLAppend<Obj<As...>, Obj<Args...>>
		{
			using type = Obj<As..., Args...>;
		};
	}
	/**
	 * @brief Append one template template list to another template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam List The template template list.
	 * @tparam Obj The list to append.
	 */
	template <typename List, typename Obj>
	using TTLAppend = typename Impl::TTLAppend<List, Obj>::type;
	/**
	 * @brief Append one template template list to another template template list if a condition is met.
	 * 
	 * @ingroup Aliases
	 * @tparam Cond The condition.
	 * @tparam List The list to append to.
	 * @tparam Obj The list to append.
	 */
	template <bool Cond, typename List, typename Obj>
	using TTLAppendIf = std::conditional_t<Cond, TTLAppend<List, Obj>, List>;
}