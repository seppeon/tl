#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Lst, size_t I, typename Sub, typename Buf = Hz::TLEmpty<Lst>>
		struct ReplaceAt;
		template <template <typename...> class As, typename First, typename ... Args, typename Sub, typename ... Tail>
		struct ReplaceAt<As<First, Args...>, 0, Sub, As<Tail...>>
		{
			using type = As<Tail..., Sub, Args...>;
		};
		template <template <typename...> class As, typename First, typename ... Args, size_t I, typename Sub, typename ... Tail>
		struct ReplaceAt<As<First, Args...>, I, Sub, As<Tail...>>
		{
			using type = typename ReplaceAt<As<Args...>, (I - 1), Sub, As<Tail..., First>>::type;
		};
	}
	/**
	 * @brief Replace the type at index `I` of the type list with `Sub`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The type list to modify.
	 * @tparam I The index to modify.
	 * @tparam Sub The new value at index `I`.
	 */
	template <typename List, size_t I, typename Sub>
	using TLReplaceAt = typename Impl::ReplaceAt<List, I, Sub>::type;
	/**
	 * @brief Replace the last element in a type list with `Sub`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to modify.
	 * @tparam Sub The type to set the last type of `List`.
	 */
	template <typename List, typename Sub>
	using TLReplaceBack = TLReplaceAt<List, TLLength<List> - 1, Sub>;
	/**
	 * @brief Replace the first element in a type list with `Sub`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to modify.
	 * @tparam Sub The type to set the first type of `List`.
	 */
	template <typename List, typename Sub>
	using TLReplaceFront = TLReplaceAt<List, 0, Sub>;
}