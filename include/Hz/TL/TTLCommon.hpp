#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	#define HZ_TTL_ARG template <typename...> class
	/**
	 * @brief A template template type.
	 * 
	 * @tparam A single template template parameter.
	 */
	template <HZ_TTL_ARG T>
	struct TType {};
	/**
	 * @brief A list of template template parameters.
	 * 
	 * @tparam Args... The template template types.
	 */
	template <HZ_TTL_ARG ... Args>
	struct TTL{};
	/**
	 * @brief An empty template template list.
	 * @ingroup Aliases
	 */
	using TTLEmptyList = TTL<>;

	namespace Impl
	{
		template <typename TT, typename ... Args>
		struct TTypeApply;
		template <HZ_TTL_ARG TTy, typename ... Args>
		struct TTypeApply<TType<TTy>, Args...>
		{
			using type = TTy<Args...>;
		};

		template <template <HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Args>
		auto TTLFrontImpl(Type<As<Front, Args...>>)->TType<Front>;
		template <template <HZ_TTL_ARG ...> class As, HZ_TTL_ARG ... Args>
		auto TTLBackImpl(Type<As<Args...>>)->decltype((TType<Args>{},...));
		template<template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG ... Args>
		auto TTLEmptyImpl(Type<As<Args...>>)->As<>;
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Args>
		auto TTLPopFrontImpl(Type<As<Front, Args...>>)->As<Args...>;
		template <typename Lst, typename Output = decltype(TTLEmptyImpl(Type<Lst>{}))>
		struct TTLPopBackImpl;
		template <template <HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Outs>
		struct TTLPopBackImpl<As<Front>, As<Outs...>> { using type = As<Outs...>; };
		template <template <HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Items, HZ_TTL_ARG ... Outs>
		struct TTLPopBackImpl<As<Front, Items...>, As<Outs...>> { using type = typename TTLPopBackImpl<As<Items...>, As<Outs..., Front>>::type; };
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG ... Args, HZ_TTL_ARG Front>
		auto TTLPushFrontImpl(Type<As<Args...>>, TType<Front>)->As<Front, Args...>;
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG ... Args, HZ_TTL_ARG Front>
		auto TTLPushBackImpl(Type<As<Args...>>, TType<Front>)->As<Args..., Front>;
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG ... Args>
		constexpr auto TTLLengthImpl(Type<As<Args...>>)->std::size_t { return sizeof...(Args); }
	}
	/**
	 * @brief Get an empty template template template list that matches the type of a given template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T the template template list to extract the type of.
	 */
	template<typename T>
	using TTLEmpty = decltype(Impl::TTLEmptyImpl(Type<T>{}));
	/**
	 * @brief Apply some types to the template template parameter `T`. 
	 * 
	 * @ingroup Aliases
	 * @tparam T The type of the template template parameter.
	 * @tparam Args The arguments to apply to the template template parameter.
	 */
	template <typename T, typename ... Args>
	using TTypeApply = typename Impl::TTypeApply<T, Args...>::type;
	/**
	 * @brief Get the first item in the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template <typename T>
	using TTLFront = decltype(Impl::TTLFrontImpl(Type<T>{}));
	/**
	 * @brief Get the last item in the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template<typename T>
	using TTLBack = decltype(Impl::TTLBackImpl(Type<T>{}));
	/**
	 * @brief Remove the first item in the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template<typename T>
	using TTLPopFront = decltype(Impl::TTLPopFrontImpl(Type<T>{}));
	/**
	 * @brief Remove the last item in the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template<typename T>
	using TTLPopBack = typename Impl::TTLPopBackImpl<T>::type;
	/**
	 * @brief Add an item to the front of the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template <typename List, HZ_TTL_ARG Arg>
	using TTLPushFront = decltype(Impl::TTLPushFrontImpl(Type<List>{}, TType<Arg>{}));
	/**
	 * @brief Add an item to the back of the template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam T The list.
	 */
	template <typename List, HZ_TTL_ARG Arg>
	using TTLPushBack = decltype(Impl::TTLPushBackImpl(Type<List>{}, TType<Arg>{}));
	/**
	 * @brief Add an item to the front of the template template list, if a condition (`Cond`) is true.
	 * 
	 * @ingroup Aliases
	 * @tparam Cond The condition.
	 * @tparam List The list to add an item to.
	 * @tparam Obj The item to add to the list.
	 */
	template <bool Cond, typename List, HZ_TTL_ARG Obj>
	using TTLPushFrontIf = std::conditional_t<Cond, TTLPushFront<List, Obj>, List>;
	/**
	 * @brief Add an item to the back of the template template list, if a condition (`Cond`) is true.
	 * 
	 * @ingroup Aliases
	 * @tparam Cond The condition.
	 * @tparam List The list to add an item to.
	 * @tparam Obj The item to add to the list.
	 */
	template <bool Cond, typename List, HZ_TTL_ARG Obj>
	using TTLPushBackIf = std::conditional_t<Cond, TTLPushBack<List, Obj>, List>;
	/**
	 * @brief Push an item item to the front of the list, the pushed item depending on a condition.
	 * 
	 * @ingroup Aliases
	 * @tparam Cond The condition.
	 * @tparam List The list to add to an item into.
	 * @tparam True The type to add to the list when `Cond` is true.
	 * @tparam False The type to add to the list when `Cond` is false.
	 */
	template <bool Cond, typename List, HZ_TTL_ARG True, HZ_TTL_ARG False>
	using TTLPushFrontIfElse = std::conditional_t<Cond, TTLPushFront<List, True>, TTLPushFront<List, False>>;
	/**
	 * @brief Push an item item to the back of the list, the pushed item depending on a condition.
	 * 
	 * @ingroup Aliases
	 * @tparam Cond The condition.
	 * @tparam List The list to add to an item into.
	 * @tparam True The type to add to the list when `Cond` is true.
	 * @tparam False The type to add to the list when `Cond` is false.
	 */
	template <bool Cond, typename List, HZ_TTL_ARG True, HZ_TTL_ARG False>
	using TTLPushBackIfElse = std::conditional_t<Cond, TTLPushBack<List, True>, TTLPushBack<List, False>>;
	/**
	 * @brief Get the number of elements in the template template list.
	 * 
	 * @ingroup Variables
	 * @tparam T The template template list.
	 */
	template <typename T>
	inline constexpr size_t ttl_length_v = Impl::TTLLengthImpl(Type<T>{});
}