#pragma once
/**
 * @file TL.hpp
 * @author David Ledger (davidledger@live.com.au)
 * @brief A type list library.
 * 
 * @defgroup Aliases Aliases that assist in manipulating type lists.
 * @defgroup Concepts Concepts that utilize type lists.
 * @defgroup Variables Inline variables providing data about type lists.
 */
#include "Hz/TL/TLAllSame.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLAppend.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLAppendN.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLApplyTrait.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLAssignAt.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLAtIndex.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLCartesianProduct.hpp"	// IWYU pragma: keep
#include "Hz/TL/TLCombine.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLCommon.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLContains.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLConvertible.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLCountIf.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLFilter.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLFlatten.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLForEach.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLHalve.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLIndexOf.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLInsertAt.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLInsertSorted.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLMatchIndexes.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLMatchOn.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLMerge.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLRemoveAll.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLRemoveAt.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLRemoveDuplicates.hpp"	// IWYU pragma: keep
#include "Hz/TL/TLRemoveFirst.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLRemoveN.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLRemoveOverlap.hpp"	// IWYU pragma: keep
#include "Hz/TL/TLReplace.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLReplaceAt.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLReverse.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLSort.hpp"				// IWYU pragma: keep
#include "Hz/TL/TLSplit.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLTranslate.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLTypeArray.hpp"		// IWYU pragma: keep
#include "Hz/TL/TLUnique.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLUnapply.hpp"			// IWYU pragma: keep
#include "Hz/TL/TLOverlap.hpp"			// IWYU pragma: keep