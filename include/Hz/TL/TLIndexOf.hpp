#pragma once
#include <cstddef>
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <typename List, typename Match, size_t Index = 0>
		struct IndexOf;
		template <template<typename...> class As, typename head, typename ... args, typename Match, size_t Index>
			requires (std::is_same_v<head, Match>)
		struct IndexOf<As<head, args...>, Match, Index>
		{
			static constexpr auto value = Index;
		};
		template <template<typename...> class As, typename head, typename ... tail, typename Match, size_t Index>
		struct IndexOf<As<head, tail ... >, Match, Index>
		{
			static constexpr auto value = IndexOf<As<tail ... >, Match, Index + 1>::value;
		};
	}
	/**
	 * @brief Get the index of a particular type in a type list.
	 *
	 * @ingroup Variables
	 * 
	 * @tparam List The list to search for `Match` in.
	 * @tparam Match The type to search for in `List`.
	 */
	template <typename List, typename Match>
	inline constexpr size_t TLIndexOf = Impl::IndexOf<List, Match>::value;
}