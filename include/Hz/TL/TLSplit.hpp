#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <std::size_t I, typename Lhs, typename Rhs>
		struct Split;
		template <template <typename...> class As, typename ... Lhss, typename ... Rhss>
		struct Split<0, As<Lhss...>, As<Rhss...>>
		{
			struct type
			{
				using lhs = As<Lhss...>;
				using rhs = As<Rhss...>;
			};
		};
		template <template <typename...> class As, std::size_t I, typename ... Lhss, typename Front, typename ... Rhss> requires (I != 0)
		struct Split<I, As<Lhss...>, As<Front, Rhss...>>
		{
			using lhs = As<Lhss..., Front>;
			using rhs = As<Rhss...>;
			using type = typename Split<I - 1, lhs, rhs>::type;
		};
	}
	/**
	 * @brief Split a type list at index `I`.
	 * 
	 * @tparam List The list to split.
	 * @tparam I The index that split will take place.
	 */
	template <typename List, std::size_t I>
	class TLSplit
	{
		using impl = typename Impl::Split<I, TLEmpty<List>, List>::type;
	public:
		using lhs = typename impl::lhs;
		using rhs = typename impl::rhs;
	};
}