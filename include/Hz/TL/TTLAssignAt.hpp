#pragma once
#include "Hz/TL/TTLCommon.hpp"
#include <cstddef>
#include <utility>

namespace Hz
{
	namespace Impl
	{
		template <typename List, typename OutList, std::size_t I, HZ_TTL_ARG T>
		struct TTLAssignAt;
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Args, HZ_TTL_ARG ... Outs, std::size_t I, HZ_TTL_ARG T>
		struct TTLAssignAt<As<Front, Args...>, As<Outs...>, I, T>
		{
			using type = typename TTLAssignAt
			<
				As<Args...>,
				As<Outs..., Front>,
				I - 1,
				T
			>::type;
		};
		template <template<HZ_TTL_ARG ...> class As, HZ_TTL_ARG Front, HZ_TTL_ARG ... Args, HZ_TTL_ARG ... Outs, HZ_TTL_ARG T>
		struct TTLAssignAt<As<Front, Args...>, As<Outs...>, 0, T>
		{
			using type = As<Outs..., T, Args...>;
		};
	}
	/**
	 * @brief Set the element at index `I` of list `List` to the template template `T`.
	 * 
	 * @ingroup Aliases
	 * @tparam List The list to modify.
	 * @tparam I The index to modify.
	 * @tparam T The new template template parameter at index `I`.
	 */
	template <typename List, std::size_t I, HZ_TTL_ARG T>
	using TTLAssignAt = typename Impl::TTLAssignAt<List, TTLEmpty<List>, I, T>::type;
}