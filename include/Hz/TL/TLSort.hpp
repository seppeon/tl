#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLInsertSorted.hpp"

namespace Hz
{
	namespace Impl
	{
		template < typename input, typename ranker >
		class TLSort;

		template <template <typename ...> class As, typename ... Args, typename ranker >
		class TLSort<As<Args...>, ranker>
		{
		private:
			using input = As<Args...>;
			using empty_list = As<>;

			template <typename ...>
			struct Looper;
			template <typename types>
			struct Looper<types, empty_list>
			{
				using type = empty_list;
			};
			template <typename types, typename head>
			struct Looper<types, As<head>>
			{
				using type = TLInsertSorted<types, head, ranker>;
			};
			template <typename types, typename head, typename ... tail>
				requires(sizeof...(tail) > 0)
			struct Looper<types, As<head, tail...>>
			{
				using appended = TLInsertSorted<types, head, ranker>;
				using type = typename Looper<appended, As<tail...>>::type;
			};
		public:
			using type	= typename Looper<empty_list, input>::type;
		};
	}
	/**
	 * @brief Sort a given type list, in the order defined by `Ranker` (see @ref Hz::TypeRankPredicate for an example ranker).
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to sort.
	 * @tparam Ranker The ranker to use to order types.
	 */
	template <typename List, typename Ranker>
	using TLSort = typename Impl::TLSort<List, Ranker>::type;
}