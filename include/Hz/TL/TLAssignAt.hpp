#pragma once
#include <cstddef>
#include <utility>

namespace Hz
{
	namespace Impl
	{
		template <typename List, typename Seq, size_t I, typename T>
		struct AssignAtImpl;
		template <template<typename...> class As, typename ... Args, size_t ... I, size_t Is, typename T>
		struct AssignAtImpl<As<Args...>, std::index_sequence<I...>, Is, T>
		{
			using type = As<std::conditional_t<(I == Is), T, Args>...>;
		};

		template <typename List, size_t I, typename T>
		struct AssignAt;
		template <template<typename...> class As, typename ... Args, size_t I, typename T>
		struct AssignAt<As<Args...>, I, T>
		{
			using type = typename AssignAtImpl<As<Args...>, std::make_index_sequence<sizeof...(Args)>, I, T>::type;
		};
	}
	/**
	 * @brief Set the value of the type list `List` at index `I` to `T`.
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam List The list to modify.
	 * @tparam I The index of the list to modify.
	 * @tparam T The new value of the type at the index `I`.
	 */
	template <typename List, size_t I, typename T>
	using TLAssignAt = typename Impl::AssignAt<List, I, T>::type;
}