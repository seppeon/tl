#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
    namespace Impl
    {
        template <typename Input, typename Output = Hz::TLEmpty<Input>>
        struct Reverse;
        template <template <typename...> class As, typename ... Args>
        struct Reverse<As<>, As<Args...>>
        {
            using type = As<Args...>;
        };
        template <template <typename...> class As, typename T, typename... Ts, typename ... Args>
        struct Reverse<As<T, Ts...>, As<Args...>>
        {
            using type = typename Reverse<As<Ts...>, As<T, Args...>>::type;
        };
    }
    /**
     * @brief Reverse the order of a type list.
     *
     * @ingroup Aliases
     * 
     * @tparam List The list to reverse.
     */
    template <typename List>
    using TLReverse = typename Impl::Reverse<List>::type;
}