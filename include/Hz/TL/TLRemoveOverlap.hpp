#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLContains.hpp"
#include "Hz/TL/TLRemoveFirst.hpp"

namespace Hz
{
	template <typename A, typename B>
	struct RemoveOverlapResult
	{
		using lhs = A;
		using rhs = B;
	};

	namespace Impl
	{
		template <typename A, typename B, typename W = Hz::TLEmpty<A>>
		struct RemoveOverlap;
		template <template<typename...> class Ls, typename ... Bs, typename ... Ws>
		struct RemoveOverlap<Ls<>, Ls<Bs...>, Ls<Ws...>>
		{
			using type = RemoveOverlapResult<Ls<Ws...>, Ls<Bs...>>;
		};
		template <template<typename...> class Ls, typename Af, typename ... As, typename ListB, typename ... Ws>
		struct RemoveOverlap<Ls<Af, As...>, ListB, Ls<Ws...>>
		{
			using type = typename RemoveOverlap<Ls<As...>, ListB, Ls<Ws..., Af>>::type;
		};
		template <template<typename...> class Ls, typename Af, typename ... As, typename ListB, typename ... Ws>
			requires (Hz::TLContains<Af, ListB>)
		struct RemoveOverlap<Ls<Af, As...>, ListB, Ls<Ws...>>
		{
			using type = typename RemoveOverlap<Ls<As...>, TLRemoveFirst<ListB, Af>, Ls<Ws...>>::type;
		};
	}
	/**
	 * @brief Remove the "Overlap" from the `Lhs` type list.
	 *
	 * See @ref Hz::TLOverlap for more information about what an overlapping type list is.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Lhs The side to remove the overlap from.
	 * @tparam Rhs The side which will overlap will be compared against.
	 */
	template <typename Lhs, typename Rhs>
	using TLRemoveOverlap = typename Impl::RemoveOverlap<Lhs, Rhs>::type;
}