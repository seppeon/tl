#pragma once
#include "Hz/TL/TLCommon.hpp"
#include <utility>

namespace Hz
{
	namespace Impl
	{
		template <std::size_t, typename T>
		using GetT = T;

		template<typename T, template <typename ...> class As, size_t ... Is>
		auto TypeArray(std::index_sequence<Is...>) -> As<GetT<Is, T>...>;
	}
	/**
	 * @brief Produce a type list with `T` repeated `N` times.
	 *
	 * @ingroup Aliases 
	 *
	 * @tparam T The type of elements of the type list.
	 * @tparam N The number of times `T` is repeated.
	 * @tparam As The type of the type list.
	 */
	template <typename T, size_t N, template <typename ...> class As = TL>
	using TLTypeArray = decltype(Impl::TypeArray<T, As>(std::make_index_sequence<N>{}));
} 