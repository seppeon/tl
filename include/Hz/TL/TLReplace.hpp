#pragma once
#include "Hz/TL/TLForEach.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename, typename from, typename to>
		struct ReplacePredicate;

		template <template <typename...> class As, typename from, typename to>
		struct ReplacePredicate<As<>, from, to>
		{
			template <typename type>
			static constexpr bool const is_same = std::is_same<from, type>::value;

			template <typename type>
			using process = TLForEachPredicate<typename std::conditional<is_same<type>, to, type>::type, true, As<>>;
		};
	};
	/**
	 * @brief Replace type `From` with `To` in the given type list.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to modify.
	 * @tparam From The type to change to the `To` type.
	 * @tparam To The type that will replace `From` types.
	 */
	template < typename List, typename From, typename To >
	using TLReplace = TLForEach<List, Impl::ReplacePredicate<TLEmpty<List>, From, To>>;
}