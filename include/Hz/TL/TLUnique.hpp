#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename List>
		struct TLUnique;
		template <template <typename ...> class As>
		struct TLUnique<As<>>
		{
			static constexpr bool has_dups = false;
			static constexpr bool value = true;
		};
		template <template <typename ...> class As, typename Front>
		struct TLUnique<As<Front>>
		{
			static constexpr bool has_dups = false;
			static constexpr bool value = true;
		};
		template <template <typename ...> class As, typename Front, typename ... Args>
		struct TLUnique<As<Front, Args...>>
		{
			static constexpr bool has_dups = ( ( std::is_same_v<Front, Args> or ... ) or TLUnique<As<Args...>>::has_dups );
			static constexpr bool value = not has_dups;
		};
	}
	/**
	 * @brief Checks if a type list has only unique types.
	 *
	 * @ingroup Concepts 
	 * 
	 * @tparam List The list to check.
	 */
	template <typename List>
	concept TLUnique = Impl::TLUnique<List>::value;
	/**
	 * @brief Checks if template parameters are all unique types.
	 *
	 * @ingroup Concepts 
	 * 
	 * @tparam Args... The types to check.
	 */
	template <typename ... Args>
	concept UniquePack = TLUnique<TL<Args...>>;
}