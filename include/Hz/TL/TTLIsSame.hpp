#pragma once
#include "Hz/TL/TTLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <HZ_TTL_ARG Lhs, HZ_TTL_ARG Rhs>
		struct TTLIsSame : std::false_type {};

		template <HZ_TTL_ARG Lhs>
		struct TTLIsSame<Lhs, Lhs> : std::true_type {};
	}
	/**
	 * @brief Check if two template template parameters are the same (ie. type of `lhs == rhs`).
	 * 
	 * @ingroup Variables
	 * @tparam Lhs The left hand argument.
	 * @tparam Rhs The right hand argument.
	 */
	template <HZ_TTL_ARG Lhs, HZ_TTL_ARG Rhs>
	inline constexpr bool ttl_is_same_v = Impl::TTLIsSame<Lhs, Rhs>::value;
}