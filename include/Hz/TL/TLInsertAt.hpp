#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Lst, size_t I, typename Sub, typename Buf = Hz::TLEmpty<Lst>>
		struct InsertAt;
		template <template<typename...> class As, typename ... Args, typename Sub, typename ... Tail>
		struct InsertAt<As<Args...>, 0, Sub, As<Tail...>>
		{
			using type = As<Tail..., Sub, Args...>;
		};
		template <template<typename...> class As, size_t I, typename Sub, typename ... Tail>
			requires (I != 0)
		struct InsertAt<As<>, I, Sub, As<Tail...>>
		{
			using type = As<Tail..., Sub>;
		};
		template <template<typename...> class As, typename First, typename ... Args, size_t I, typename Sub, typename ... Tail>
			requires (I != 0)
		struct InsertAt<As<First, Args...>, I, Sub, As<Tail...>>
		{
			using type = typename InsertAt<As<Args...>,(I - 1),Sub,As<Tail..., First>>::type;
		};
	}
	/**
	 * @brief Insert a type `Sub` into the type list `List`, at index `I`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list to insert into.
	 * @tparam I The index of the list to insert at.
	 * @tparam Sub The type to insert.
	 */
	template <typename List, size_t I, typename Sub>
	using TLInsertAt = typename Impl::InsertAt<List, I, Sub>::type;
}