#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLCountIf.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename List, typename Result = Hz::TLEmpty<List>>
		struct RemoveDuplicates;
		template <template<typename...> class As, typename ... RArgs>
		struct RemoveDuplicates<As<>, As<RArgs...>>
		{
			using type = As<RArgs...>;;
		};
		template <template<typename...> class As, typename First, typename ... Args, typename ... RArgs>
		struct RemoveDuplicates<As<First, Args...>, As<RArgs...>>
		{
		private:
			using check_item_t = First;
			using scan_list_t = As<Args...>;
			using result_list_t = As<RArgs...>;

			static constexpr bool is_duplicate_v = Hz::TLMatchCount<scan_list_t, check_item_t> > 0;
			using remove_item_t = typename RemoveDuplicates<scan_list_t, result_list_t>::type;
			using keep_item_t = typename RemoveDuplicates<scan_list_t, As<RArgs..., check_item_t>>::type;
		public:
			using type = std::conditional_t<is_duplicate_v, remove_item_t, keep_item_t>;
		};
	}
	/**
	 * @brief Remove duplicate types from a type list.
	 *
	 * @ingroup Aliases 
	 * 
	 * @tparam List The list to remove duplicates from.
	 */
	template <typename List>
	using TLRemoveDuplicates = typename Impl::RemoveDuplicates<List>::type;
}