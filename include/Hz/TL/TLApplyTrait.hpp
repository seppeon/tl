#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename List, template<typename...> class Trait>
		struct ApplyTrait;

		template <template<typename...> class As, typename ... Items, template<typename...> class Trait>
			requires(not requires{ typename Trait<Hz::TLFront<TL<Items...>>>::type; })
		struct ApplyTrait<As<Items...>, Trait>
		{
			using type = As<Trait<Items>...>;
		};
		template <template<typename...> class As, typename ... Items, template<typename...> class Trait>
		struct ApplyTrait<As<Items...>, Trait>
		{
			using type = As<typename Trait<Items>::type...>;
		};
	}
	/**
	 * @brief Apply a trait defined by `Trait` to the types in the type list defined by `List`.
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam List The type list to apply the trait.
	 * @tparam Trait The trait to apply (for example, `std::add_pointer_t`).
	 */
	template <typename List, template<typename...> class Trait>
	using TLApplyTrait = typename Impl::ApplyTrait<List, Trait>::type;
}