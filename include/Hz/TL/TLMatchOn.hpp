#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template < typename ... >
		struct MatchOn;
		template <template<typename...> class As, typename predicate, typename head >
		struct MatchOn<predicate, As< head > >
		{
			static constexpr bool value = predicate::template match<head>;
			using type = std::conditional_t<value, head, As<>>;
		};
		template <template<typename...> class As, typename predicate, typename head, typename ... tail >
		struct MatchOn< predicate, As< head, tail ... > >
		{
			using rest = As< tail ...>;
			using more = MatchOn< predicate, rest >;

			static constexpr bool is_match = predicate::template match< head >;
			static constexpr bool value = (is_match || more::value);

			using type = std::conditional_t< is_match, head, typename more::type >;
		};
	}
	/**
	 * @brief An example match predicate for use with `TLMatchOn`.
	 * 
	 * @tparam T The type to match on.
	 */
	template < typename T >
	struct BasicMatchOn
	{
		template < typename input >
		static constexpr bool match = std::is_same<T, input>::value;
	};
	/**
	 * @brief Find the first type which matches the given predicate.
	 *
	 * @ingroup Aliases 
	 * 
	 * @tparam List The list to look for a match in.
	 * @tparam Predicate The predicate to use.
	 */
	template <typename List, typename Predicate>
	using TLMatchOn = typename Impl::MatchOn<Predicate, List>::type;
};