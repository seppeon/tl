#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLCombine.hpp"
#include "Hz/TL/TLFlatten.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename types, typename predicate>
		class TLForEach;

		template <template <typename...> class As, typename ... Args, typename predicate>
		class TLForEach<As<Args...>, predicate>
		{
		private:
			using types = As<Args...>;
			using empty_list = As<>;

			template <typename d, typename ... items >
			struct Looper;
			template <typename d>
			struct Looper<d, empty_list>
			{
			private:
				using result = typename predicate::template process<empty_list>;
				using last = typename result::last;
			public:
				using type = Hz::TLFlatten<As, As<last>>;
			};
			template <typename d, typename item>
			struct Looper<d, As<item>>
			{
			private:
				using result	= typename predicate::template process<item>;
				using front		= typename result::type;
				using last		= typename result::last;
				using rest		= std::conditional_t<result::next, last, empty_list>;
			public:
				using type = Hz::TLCombine<As, front, rest>;
			};
			template <typename d, typename ... items>
				requires(sizeof...(items)>1)
			struct Looper<d, As<items ...>>
			{
			private:
				using list		= As< items ... >;
				using result	= typename predicate::template process<Hz::TLFront<list>>;
				using front		= typename result::type;
				using rest_r	= Hz::TLPopFront<list>;
				using rest		= std::conditional_t<result::next, typename Looper<void, rest_r>::type, rest_r>;
			public:
				using type = Hz::TLCombine<As, front, rest>;
			};
		public:
			using type = typename Looper<void, types>::type;
		}; 
	}
	/**
	 * @brief The type that must be provided by a `TLForEach` predicate's `process` type alias member.
	 * 
	 * @tparam T The type of that will be replace the currently traversing element.
	 * @tparam Next Whether or not to continue traversal to the `Next` element.
	 * @tparam L The type to place at the end of traversal.
	 */
	template<typename T, bool Next, typename L>
	struct TLForEachPredicate
	{
		static constexpr bool next = Next;
		using type = T;
		using last = L;
	};
	/**
	 * @brief Traverse through a type list, applying a predicate to each member.
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam List The type list to traverse.
	 * @tparam Predicate The predicate to apply.
	 */
	template <typename List, typename Predicate>
	using TLForEach = typename Impl::TLForEach<List, Predicate>::type;
};