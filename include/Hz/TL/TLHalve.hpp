#pragma once
#include "Hz/TL/TLCombine.hpp"

namespace Hz
{
	/**
	 * @brief Split a type list into two halves.
	 * 
	 * @tparam List The list to split.
	 */
	template <typename List>
	class TLHalve;
	/**
	 * @brief Split a type list into two halves.
	 *
	 * @tparam As The type of the type list.
	 * @tparam Ts... The types in the type list.
	 */
	template <template <typename...> class As, typename ... Ts>
	class TLHalve<As<Ts...>>
	{
	private:
		using types = As<Ts...>;
		using empty_list = As<>;

		template <typename ...>
		struct RhsToggler;
		template <typename ...>
		struct LhsToggler;
		template <typename lhs, typename rhs>
		struct LhsToggler<lhs, rhs, empty_list>
		{
			using lhs_t = lhs;
			using rhs_t = rhs;
		};
		template <typename lhs, typename rhs>
		struct RhsToggler<lhs, rhs, empty_list>
		{
			using lhs_t = lhs;
			using rhs_t = rhs;
		};
		template <typename lhs, typename rhs, typename remaining>
		struct LhsToggler<lhs, rhs, remaining>
		{
			using item = TLFront<remaining>;
			using new_remaining = TLPopFront<remaining>;
			using new_lhs = TLCombine<As, lhs, item>;
			using new_rhs = rhs;

			using new_toggler = RhsToggler<new_lhs, new_rhs, new_remaining>;

			using lhs_t = typename new_toggler::lhs_t;
			using rhs_t = typename new_toggler::rhs_t;
		};
		template <typename lhs, typename rhs, typename remaining>
		struct RhsToggler<lhs, rhs, remaining>
		{
			using item = TLFront<remaining>;
			using new_remaining = TLPopFront<remaining>;
			using new_lhs = lhs;
			using new_rhs = TLCombine<As, rhs, item>;

			using new_toggler = LhsToggler<new_lhs, new_rhs, new_remaining>;

			using lhs_t = typename new_toggler::lhs_t;
			using rhs_t = typename new_toggler::rhs_t;
		};

		using toggler = LhsToggler<empty_list, empty_list, types>;
	public:
		using lhs =typename toggler::lhs_t;
		using rhs =typename toggler::rhs_t;
	};
};