#pragma once
#include "Hz/TL/TLAtIndex.hpp"
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLFilter.hpp"
#include "Hz/TL/TLAppend.hpp"
#include "Hz/TL/TLTranslate.hpp"
#include <array>
#include <cstddef>
#include <type_traits>
#include <utility>

namespace Hz
{
	namespace Impl
	{
		template <typename ... Ls>
		class TLCartesianProduct
		{
			using array_type = std::array<std::size_t, sizeof...(Ls)>;
			static constexpr size_t count = (Hz::TLLength<std::remove_cvref_t<Ls>> * ...);
			using array_combinations_type = std::array<array_type, count>;

			static constexpr auto GetCombinations() -> array_combinations_type
			{
				array_type const limits{ Hz::TLLength<std::remove_cvref_t<Ls>>... };
				array_combinations_type output{};
				for (size_t i = 1; i < count; ++i)
				{
					auto & curr = output[i];
					curr = output[i - 1];
					size_t carry = 1;
					for (size_t x = 0; x < sizeof...(Ls); ++x)
					{
						auto const limit = limits[x];
						auto const value = curr[x] + carry;
						if (value < limit)
						{
							curr[x] = value;
							carry = 0;
							break;
						}
						else
						{
							curr[x] = 0;
							carry = 1;
						}
					}
				}
				return output;
			}
			static constexpr auto combinations = GetCombinations();

			template <size_t I, size_t ... Is>
			static constexpr auto GetCombinationType(std::index_sequence<Is...>)
			{
				return Hz::TL<Hz::TLAtIndex<Ls, combinations[I][Is]>...>{};
			}
			template <size_t I>
			using combination_type = decltype(GetCombinationType<I>(std::make_index_sequence<sizeof...(Ls)>()));

			template <size_t ... Is>
			static constexpr auto GetCombinationTypes(std::index_sequence<Is...>)
			{
				return Hz::TL<combination_type<Is>...>{};
			}
		public:
			using type = decltype(GetCombinationTypes(std::make_index_sequence<count>()));
		};

		template <typename F>
		class InvocableFilter
		{
			using callable_list = TL<F>;

			template <typename T>
			using add_args = Hz::TLAppend<callable_list, T>;
		public:
			template <typename T>
			static constexpr bool Test = Hz::TLTranslate<add_args<T>, std::is_invocable>::value;
		};
	}
	/**
	 * @brief Get the cartesian product of types.
	 *
	 * For example:
	 * @li `TL<int, short>` 
	 * @li `TL<bool, int>`
	 *
	 * would have a cartesian product of:
	 * @code {.cpp}
	 * using result = TL<
	 * 		TL<int, bool>,
	 * 		TL<short, bool>,
	 * 		TL<int, int>,
	 * 		TL<short, int>
	 * >;
	 * @endcode
	 *
	 * If instead we had three type lists:
	 * @li `TL<int, short>` 
	 * @li `TL<bool, int>`
	 * @li `TL<long, float>`
	 *
	 * It results in to the following:
	 * @code {.cpp}
	 * using result = TL<
	 * 		TL<int, bool, long>,
	 * 		TL<int, int, long>,
	 * 		TL<short, bool, long>,
	 * 		TL<short, int, long>,
	 * 		TL<int, bool, float>,
	 * 		TL<int, int, float>,
	 * 		TL<short, bool, float>,
	 * 		TL<short, int, float>
	 * >;
	 * @endcode
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam Ls... The list of type lists (or anything that is a valid template template parameter with a type pack).
	 */
	template <typename ... Ls>
	using TLCartesianProduct = typename Impl::TLCartesianProduct<Ls...>::type;
	/**
	 * @brief Calculates the `Hz::TLCartesianProduct` of a list of type lists, then removes any type lists that cannot be applied as arguments to `F`.
	 * 
	 * For example, if we have a function type of `F = int (*)(int *, int)`, and we are given:
	 * @li `TL<int, short>` 
	 * @li `TL<bool, int>`
	 *
	 * the result would be an empty type list `TL<>`. This is because none of the cartesian products can be applied as arguments to a function of type `F`.
	 *
	 * Instead, if the types were:
	 * @li `TL<int *, short>`
	 * @li `TL<bool, int>`
	 *
	 * the result would be an empty type list `TL<TL<int *, bool>, TL<int *, int>>`. This is because none of the cartesian products can be applied as arguments to a function of type `F`.
	 * 
	 * @ingroup Aliases
     *
	 * @tparam F The function type.
	 * @tparam Ls... The list of type lists (or anything that is a valid template template parameter with a type pack).
	 */
	template <typename F, typename ... Ls>
	using TLInvocableCartesianProduct = TLFilter<TLCartesianProduct<Ls...>, Impl::InvocableFilter<F>>;
}