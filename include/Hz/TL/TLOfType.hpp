#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <template<typename...> class Match, typename List>
		struct TLOfType : std::false_type {};
		template <template<typename...> class Match, typename ... Args>
		struct TLOfType<Match, Match<Args...>> : std::true_type {};
	}
	/**
	 * @brief Check if a type list is of a particular type.
	 *
	 * @ingroup Concepts 
	 *
	 * For example:
	 * @code
	 * TLOfType<TL<int, char>, TL> 
	 * @endcode
	 * would be a match.
	 *
	 * But:
	 * @code
	 * TLOfType<TL<int, char>, std::tuple> 
	 * @endcode
	 * would not be a match.
	 *
	 * @tparam List The type list to check the type of.
	 * @tparam Match The desired type list type.
	 */
	template <typename List, template<typename...> class Match>
	concept TLOfType = Impl::TLOfType<Match, List>::value;
}