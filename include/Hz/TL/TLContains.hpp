#pragma once
#include "Hz/TL/TLCommon.hpp"
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template<typename types, typename match>
		struct Contains
		{
			static constexpr bool value = false;
		};
		template <template<typename...> class As, typename ... Args, typename Match>
		struct Contains<As<Args...>, Match>
		{
			static constexpr bool value = (std::is_same_v<Args, Match> or ...);
		};
		template <template<typename...> class As, typename ... Args, typename Match>
		struct Contains<As<Args...>, As<Match>>
		{
			static constexpr bool value = (std::is_same_v<Args, Match> or ...);
		};
		template <template<typename...> class As, typename ... Args, typename ... Matches>
		struct Contains<As<Args...>, As<Matches...>>
		{
			static constexpr bool value = (Contains<As<Args...>, As<Matches>>::value and ...);
		};
	}
	/**
	 * @brief Check if a type is contained within a list.
	 *
	 * @ingroup Concepts 
	 *
	 * @tparam T The type to check.
	 * @tparam List The list to look for the type in.
	 */
	template<typename T, typename List>
	concept TLContains = Impl::Contains<List, T>::value;
	/**
	 * @brief Check if template parameters contain a particular type.
	 *
	 * @ingroup Concepts
	 * 
	 * @tparam T The type to look for.
	 * @tparam Args... The arguments to compare with `T`.
	 */
	template<typename T, typename ... Args>
	concept TLPackContains = Impl::Contains<TL<Args...>, T>::value;
	/**
	 * @brief Check if a type(ignoring qualifiers and value categories) is contained within a list.
	 *
	 * @ingroup Concepts
	 * 
	 * @tparam T The type to look for.
	 * @tparam List The list to search for `T` in.
	 */
	template<typename T, typename List>
	concept TLContainsRemoveCvref = Impl::Contains<List, std::remove_cvref_t<T>>::value;

	namespace Impl
	{
		template<typename types, typename match>
		struct ContainsConvertible
		{
			static constexpr bool value = false;
		};
		template <template<typename...> class As, typename ... Args, typename Match>
		struct ContainsConvertible<As<Args...>, Match>
		{
			static constexpr bool value = (std::is_convertible_v<Args, Match> or ...);
		};
		template <template<typename...> class As, typename ... Args, typename Match>
		struct ContainsConvertible<As<Args...>, As<Match>>
		{
			static constexpr bool value = (std::is_convertible_v<Args, Match> or ...);
		};
		template <template<typename...> class As, typename ... Args, typename ... Matches>
		struct ContainsConvertible<As<Args...>, As<Matches...>>
		{
			static constexpr bool value = (ContainsConvertible<As<Args...>, As<Matches>>::value and ...);
		};
	}
	/**
	 * @brief Check if a type `T` is convertable to any of the types in the `List`.
	 *
	 * @ingroup Concepts
	 * 
	 * @tparam T The type to check for convertibility.
	 * @tparam List The list to search in.
	 */
	template<typename T, typename List>
	concept TLContainsConvertible = Impl::ContainsConvertible<List, T>::value;
}