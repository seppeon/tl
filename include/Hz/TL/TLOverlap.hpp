#pragma once
#include "Hz/TL/TLCommon.hpp"
#include "Hz/TL/TLContains.hpp"
#include "Hz/TL/TLRemoveFirst.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename A, typename B, typename W = TLEmpty<A>>
		struct Overlap;
		template <template<typename...> class Ls, typename ... Bs, typename ... Ws>
		struct Overlap<Ls<>, Ls<Bs...>, Ls<Ws...>>
		{
			using type = Ls<Ws...>;
		};
		template <template<typename...> class Ls, typename Af, typename ... As, typename ListB, typename ... Ws>
		struct Overlap<Ls<Af, As...>, ListB, Ls<Ws...>>
		{
			using type = typename Overlap<Ls<As...>, ListB, Ls<Ws...>>::type;
		};
		template <template<typename...> class Ls, typename Af, typename ... As, typename ListB, typename ... Ws>
			requires (TLContains<Af, ListB>)
		struct Overlap<Ls<Af, As...>, ListB, Ls<Ws...>>
		{
			using type = typename Overlap<Ls<As...>, TLRemoveFirst<ListB, Af>, Ls<Ws..., Af>>::type;
		};
	}
	/**
	 * @brief Get the types that are common between two lists (that overlap).
	 * 
	 * For example:
	 * @code
	 * TLOverlap<TL<int, char, short>, TL<short, int>>
	 * @endcode
	 * would produce a type list:
	 * @code
	 * TL<int, short>
	 * @endcode
	 * The resulting list's order is undefined, do not rely on order preservation.
	 *
	 * @ingroup Aliases
	 *
	 * @tparam Lhs The left hand list.
	 * @tparam Rhs The right hand list.
	 */
	template <typename Lhs, typename Rhs>
	using TLOverlap = typename Impl::Overlap<Lhs, Rhs>::type;
}