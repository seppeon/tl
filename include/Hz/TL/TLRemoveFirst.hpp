#pragma once
#include "Hz/TL/TLForEach.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename E, typename match, bool next_match = true, bool next_unmatch = true>
		struct RemovePredicate;

		template <template <typename...> class As, typename match, bool next_match, bool next_unmatch>
		struct RemovePredicate<As<>, match, next_match, next_unmatch>
		{
			template <typename type>
			static constexpr bool const is_same = std::is_same<match, type>::value;

			template <typename type>
			static constexpr bool const is_next()
			{
				constexpr bool same = is_same<type>;
				return (same & next_match) | ((!same) & next_unmatch);
			}

			template <typename type>
			using process = TLForEachPredicate<typename std::conditional<is_same<type>, As<>, type>::type, is_next<type>(), As<>>;
		};
	};
	/**
	 * @brief Remove the first match from the type list.
	 *
	 * @ingroup Aliases 
	 * 
	 * @tparam List The type list to remove the first match from.
	 * @tparam Match The match to remove from the type list.
	 */
	template < typename List, typename Match >
	using TLRemoveFirst = TLForEach<List, Impl::RemovePredicate<TLEmpty<List>, Match, false, true>>;
};