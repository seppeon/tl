#pragma once
#include <cstddef>

namespace Hz
{
	namespace Impl
	{
		template <typename List, size_t Index>
		struct AtIndex
		{
			using type = void;
		};
		template <template<typename...> class As, typename head, typename ... args>
		struct AtIndex<As<head, args...>, 0>
		{
			using type = head;
		};
		template <template<typename...> class As, size_t i, typename head, typename ... tail>
			requires (i > 0)
		struct AtIndex<As<head, tail ... >, i>
		{
			using tail_list = As<tail ... >;
			using type = typename AtIndex<tail_list, i - 1>::type;
		};
	}
	/**
	 * @brief Get the type in the `List` at index `I`.
	 * 
	 * @ingroup Aliases
	 * 
	 * @tparam List The list.
	 * @tparam I The index of the requested type.
	 */
	template <typename List, size_t I>
	using TLAtIndex = typename Impl::AtIndex<List, I>::type;
};