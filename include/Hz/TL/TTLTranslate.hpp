#pragma once
#include "Hz/TL/TTLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename From, template <HZ_TTL_ARG...> class To>
		struct TTLTranslate;
		template <template <HZ_TTL_ARG...> class From, HZ_TTL_ARG... Args, template <HZ_TTL_ARG...> class To>
		struct TTLTranslate<From<Args...>, To>
		{
			using type = To<Args...>;
		};
	}
	/**
	 * @brief Translate one template template list into another type of template template list.
	 * 
	 * @ingroup Aliases
	 * @tparam From The list to extract parameters.
	 * @tparam To The type of the template template list to apply the parameters from `From`.
	 */
	template <typename From, template <HZ_TTL_ARG...> class To>
	using TTLTranslate = typename Impl::TTLTranslate<From, To>::type;
}