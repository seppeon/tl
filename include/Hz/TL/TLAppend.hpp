#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Obj, typename List>
		struct TLAppend;
		template <template <typename ...> class Obj, typename ... As, typename ... Args>
		struct TLAppend<Obj<As...>, Obj<Args...>>
		{
			using type = Obj<As..., Args...>;
		};
	}
	/**
	 * @brief Add `Obj` to the end of the type list defined by `List`.
	 *
	 * @ingroup Aliases
	 *
	 * @tparam List The type list.
	 * @tparam Obj The object to append to the type list.
	 */
	template <typename List, typename Obj>
	using TLAppend = typename Impl::TLAppend<List, Obj>::type;
}