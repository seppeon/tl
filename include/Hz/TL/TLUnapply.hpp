#pragma once
#include "Hz/TL/TLCommon.hpp"

namespace Hz
{
	/**
	 * @brief Produce a type list conversion function, which takes a list (`Lst`), and provides a template aliases which procues a type list with the same type list type.
	 * 
	 * @tparam Lst The source type list (the types produced will have the same type list type).
	 */
	template <typename Lst>
	struct TLUnapply;
	/**
	 * @brief Produce a type list conversion function, which takes a list (`Lst`), and provides a template aliases which procues a type list with the same type list type.
	 * 
	 * @tparam As The type list type.
	 * @tparam Args... The arguments in the source type list (these are ignored entirely).
	 */
	template <template <typename...> class As, typename ... Args>
	struct TLUnapply<As<Args...>>
	{
		/**
		 * @brief Produce a type list with the same type list type as `As`.
		 * 
		 * @tparam Ts... The types in the new type list.
		 */
		template <typename ... Ts> 
		using type = As<Ts...>;
	};
}