#pragma once
#include "Hz/TL/TTLCommon.hpp"
#include <cstddef>

namespace Hz
{
	namespace Impl
	{
		template <typename List, std::size_t Index>
		struct TTLAtIndex
		{
			using type = void;
		};
		template <template<HZ_TTL_ARG...> class As, HZ_TTL_ARG Head, HZ_TTL_ARG ... Args>
		struct TTLAtIndex<As<Head, Args...>, 0>
		{
			using type = TType<Head>;
		};
		template <template<HZ_TTL_ARG...> class As, std::size_t i, HZ_TTL_ARG Head, HZ_TTL_ARG ... Tail>
			requires (i > 0)
		struct TTLAtIndex<As<Head, Tail ... >, i>
		{
			using tail_list = As<Tail ... >;
			using type = typename TTLAtIndex<tail_list, i - 1>::type;
		};
	}
	/**
	 * @brief Get the template template type at the index `I` of list `List`.
	 * 
	 * @ingroup Aliases
	 * @tparam List The list to extract an element.
	 * @tparam I The index of the element to extract.
	 */
	template <typename List, std::size_t I>
	using TTLAtIndex = typename Impl::TTLAtIndex<List, I>::type;
};