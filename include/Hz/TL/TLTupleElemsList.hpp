#pragma once
#include "Hz/TL/TLTypeArray.hpp"
#include "Traits/Concepts.hpp"

namespace Hz
{
	namespace Impl
	{
		template <Traits::TupleLike T>
		struct TupleElemsList
		{
			static constexpr sz pack_count = TLLength<T>;
			static constexpr sz element_count = std::tuple_size_v<T>;

			using single_element_front = TLFront<T>;
			using single_element_produce = TLTypeArray<single_element_front, element_count>;
			using multi_element_produce = TLTranslate<T, TL>;
			using type = std::conditional_t<(pack_count == element_count), multi_element_produce, single_element_produce>;
		};
	}
	/**
	 * @brief Given a type that supports structured bindings, produce a list of the types that would be bound.
	 *
	 * For example:
	 * @code
	 * std::tuple<int, char, bool>
	 * @endcode
	 * would produce a list of `TL<int, char, bool>`.
	 *
	 * @ingroup Aliases
	 *  
	 * @tparam T The type to get list.
	 */
	template <typename T>
	using TLTupleElemsList = typename TupleElemsList<T>::type;
}