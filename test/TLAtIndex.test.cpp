#include "Hz/TL/TLAtIndex.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLAtIndex", "[TL::TLAtIndex]")
{
	using namespace Hz;

	using test_list1 = TL<int, bool, char, short, unsigned>;
	using type_at_0 = TLAtIndex<test_list1, 0u>;
	using type_at_1 = TLAtIndex<test_list1, 1u>;
	using type_at_2 = TLAtIndex<test_list1, 2u>;
	using type_at_3 = TLAtIndex<test_list1, 3u>;
	using type_at_4 = TLAtIndex<test_list1, 4u>;
	using test_list2 = TL<int, bool>;
	using type2_at_0 = TLAtIndex<test_list2,0u>;
	using type2_at_1 = TLAtIndex<test_list2,1u>;
	using test_list3 = TL<int>;
	using type3_at_0 = TLAtIndex<test_list3, 0u>;

	STATIC_REQUIRE(std::is_same_v<type_at_0, int>);
	STATIC_REQUIRE(std::is_same_v<type_at_1, bool>);
	STATIC_REQUIRE(std::is_same_v<type_at_2, char>);
	STATIC_REQUIRE(std::is_same_v<type_at_3, short>);
	STATIC_REQUIRE(std::is_same_v<type_at_4, unsigned>);
	STATIC_REQUIRE(std::is_same_v<type2_at_0, int>);
	STATIC_REQUIRE(std::is_same_v<type2_at_1, bool>);
	STATIC_REQUIRE(std::is_same_v<type3_at_0, int>);
}