#include "Hz/TL/TLAppendN.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLAppendN", "[TL::TLAppendN]")
{
	using namespace Hz;

	STATIC_REQUIRE(std::is_same_v<TLAppendN<TL<>, int, 0>, TL<>>);
	STATIC_REQUIRE(std::is_same_v<TLAppendN<TL<>, int, 3>, TL<int, int, int>>);
	STATIC_REQUIRE(std::is_same_v<TLAppendN<TL<bool>, int, 0>, TL<bool>>);
	STATIC_REQUIRE(std::is_same_v<TLAppendN<TL<bool>, int, 1>, TL<bool, int>>);
	STATIC_REQUIRE(std::is_same_v<TLAppendN<TL<bool>, int, 3>, TL<bool, int, int, int>>);
}