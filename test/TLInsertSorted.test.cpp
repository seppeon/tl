#include "Hz/TL/TLInsertSorted.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLInsertSorted", "[TL::TLInsertSorted]")
{
	using namespace Hz;

	using order_list = TL<int, short, char, bool>;
	using predicate	= TypeRankPredicate<order_list>;

	using l0 = EmptyList;
	using l1 = TLInsertSorted<l0, char,	predicate>;
	using l2 = TLInsertSorted<l1, bool,	predicate>;
	using l3 = TLInsertSorted<l2, int,	predicate>;
	using l4 = TLInsertSorted<l3, short,	predicate>;

	STATIC_REQUIRE(std::is_same_v<l4, order_list>);

	using l5 = TLInsertSorted<l4, short,	predicate>;
	using l6 = TLInsertSorted<l5, bool,	predicate>;
	using l7 = TLInsertSorted<l6, int,	predicate>;

	STATIC_REQUIRE(std::is_same_v<l7, TL<int, int, short, short, char, bool, bool>>);
}