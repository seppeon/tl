#include "Hz/TL/TLRemoveDuplicates.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveDuplicates", "[TL::TLRemoveDuplicates]")
{
	using namespace Hz;

	using lista_t = TLRemoveDuplicates<TL<int, bool, double>>;
	using listb_t = TLRemoveDuplicates<TL<int, bool, double, int>>;
	using listc_t = TLRemoveDuplicates<TL<int>>;
	using listd_t = TLRemoveDuplicates<TL<>>;

	STATIC_REQUIRE(std::is_same_v<lista_t, TL<int, bool, double>>);
	STATIC_REQUIRE(std::is_same_v<listb_t, TL<bool, double, int>>);
	STATIC_REQUIRE(std::is_same_v<listc_t, TL<int>>);
	STATIC_REQUIRE(std::is_same_v<listd_t, TL<>>);
}