#include "Hz/TL/TLAppend.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLAppend", "[TL::TLAppend]")
{
	using Hz::TL;
	using Hz::TLAppend;
	STATIC_REQUIRE(std::same_as<TLAppend<TL<>, TL<>>, TL<>>);
	STATIC_REQUIRE(std::same_as<TLAppend<TL<>, TL<int>>, TL<int>>);
	STATIC_REQUIRE(std::same_as<TLAppend<TL<bool>, TL<int>>, TL<bool, int>>);
	STATIC_REQUIRE(std::same_as<TLAppend<TL<bool, int>, TL<short>>, TL<bool, int, short>>);
}