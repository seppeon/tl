#include "Hz/TL/TLReplaceAt.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLReplaceAt", "[TL::TLReplaceAt]")
{
	using namespace Hz;
	using test_list = TL<int, unsigned, short>;
	using replaced_0 = TLReplaceAt<test_list, 0, bool>;
	using replaced_1 = TLReplaceAt<test_list, 1, bool>;
	using replaced_2 = TLReplaceAt<test_list, 2, bool>;

	STATIC_REQUIRE(std::is_same_v<replaced_0, TL<bool, unsigned, short>>);
	STATIC_REQUIRE(std::is_same_v<replaced_1, TL<int, bool, short>>);
	STATIC_REQUIRE(std::is_same_v<replaced_2, TL<int, unsigned, bool>>);
}