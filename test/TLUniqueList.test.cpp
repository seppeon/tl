#include "Hz/TL/TLUnique.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLUniqueTest")
{
	using unique_list      = Hz::TL<int, short, char>;
	using non_unique_list0 = Hz::TL<int, char, short, char>;
	using non_unique_list1 = Hz::TL<char, int, short, char>;

	STATIC_REQUIRE(Hz::TLUnique<unique_list>);
	STATIC_REQUIRE(not Hz::TLUnique<non_unique_list0>);
	STATIC_REQUIRE(not Hz::TLUnique<non_unique_list1>);
}