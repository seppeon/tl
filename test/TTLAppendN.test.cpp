#include "Hz/TL/TTLAppendN.hpp"
#include "Hz/TL/TTLCommon.hpp"
#include <catch2/catch_all.hpp>
#include <tuple>

TEST_CASE("TTLAppendN", "[TL::TTLAppendN]")
{
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendN<Hz::TTLEmptyList, std::vector, 2>, Hz::TTL<std::vector, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendN<Hz::TTLEmptyList, std::vector, 0>, Hz::TTL<>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendN<Hz::TTL<std::tuple>, std::vector, 2>, Hz::TTL<std::tuple, std::vector, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendN<Hz::TTL<std::tuple>, std::vector, 0>, Hz::TTL<std::tuple>>);
}