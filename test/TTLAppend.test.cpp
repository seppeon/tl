#include "Hz/TL/TTLAppend.hpp"
#include "Hz/TL/TTLCommon.hpp"
#include <catch2/catch_all.hpp>
#include <tuple>

TEST_CASE("TTLAppend", "[TL::TTLAppend]")
{
	STATIC_REQUIRE(std::same_as<Hz::TTLAppend<Hz::TTLEmptyList, Hz::TTLEmptyList>, Hz::TTLEmptyList>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppend<Hz::TTLEmptyList, Hz::TTL<std::tuple>>, Hz::TTL<std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppend<Hz::TTL<std::vector>, Hz::TTL<std::tuple>>, Hz::TTL<std::vector, std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendIf<true, Hz::TTLEmptyList, Hz::TTL<std::tuple>>, Hz::TTL<std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendIf<true, Hz::TTL<std::vector>, Hz::TTL<std::tuple>>, Hz::TTL<std::vector, std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendIf<false, Hz::TTLEmptyList, Hz::TTL<std::tuple>>, Hz::TTLEmptyList>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAppendIf<false, Hz::TTL<std::vector>, Hz::TTL<std::tuple>>, Hz::TTL<std::vector>>);
}