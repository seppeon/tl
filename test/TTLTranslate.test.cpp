#include "Hz/TL/TTLTranslate.hpp"
#include <catch2/catch_all.hpp>
#include <list>
#include <vector>

namespace
{
	template <template <class, class> class ... Args>
	struct Test{};
}

TEST_CASE("TTLTranslate", "[TL::TTLTranslate]")
{
	using list = Hz::TTL<std::vector, std::list>;
	STATIC_REQUIRE(std::same_as<Hz::TTLTranslate<list, Test>, Test<std::vector, std::list>>);
}