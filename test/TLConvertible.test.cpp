#include "Hz/TL/TLConvertible.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLConvertible", "[TL::TLConvertible]")
{
	using namespace Hz;

	STATIC_REQUIRE(TLConvertible<TL<int, char>, TL<int, char>>);
	STATIC_REQUIRE(not TLConvertible<TL<int, char>, TL<int, char* const>>);
	STATIC_REQUIRE(not TLConvertible<TL<int, char>, TL<int>>);
}