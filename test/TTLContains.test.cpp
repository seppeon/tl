#include "Hz/TL/TTLContains.hpp"
#include <catch2/catch_all.hpp>
#include <list>
#include <queue>
#include <vector>

TEST_CASE("TTLContains", "[TL::TTLContains]")
{
	STATIC_REQUIRE(Hz::ttl_contains_v<Hz::TTL<std::vector, std::list>, std::vector>);
	STATIC_REQUIRE(Hz::ttl_contains_v<Hz::TTL<std::vector, std::list>, std::list>);
	STATIC_REQUIRE_FALSE(Hz::ttl_contains_v<Hz::TTL<std::vector, std::list>, std::queue>);
}