#include "Hz/TL/TTLTypeArray.hpp"
#include <catch2/catch_all.hpp>
#include <vector>

TEST_CASE("TTLTypeArray", "[TL::TTLTypeArray]")
{
	STATIC_REQUIRE(std::same_as<Hz::TTLTypeArray<std::vector, 0>, Hz::TTL<>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLTypeArray<std::vector, 1>, Hz::TTL<std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLTypeArray<std::vector, 2>, Hz::TTL<std::vector, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLTypeArray<std::vector, 3>, Hz::TTL<std::vector, std::vector, std::vector>>);
}