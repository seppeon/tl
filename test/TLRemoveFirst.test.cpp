#include "Hz/TL/TLRemoveFirst.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveFirst", "[TL::TLRemoveFirst]")
{
	using namespace Hz;

	using list = TL<int, short, char, int, short, int>;
	using removed_short = TLRemoveFirst<list, short>;
	STATIC_REQUIRE(std::is_same_v<removed_short, TL<int, char, int, short, int>>);
}