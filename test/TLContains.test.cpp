#include "Hz/TL/TLContains.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLContains", "[TL::TLContains]")
{
	using namespace Hz;

	using intial_list = TL<int, char, bool, short>;
	STATIC_REQUIRE(TLContains<bool, intial_list>);
	STATIC_REQUIRE(TLContains<int, intial_list>);
	STATIC_REQUIRE(TLContains<char, intial_list>);
	STATIC_REQUIRE(TLContains<short, intial_list>);
	STATIC_REQUIRE(!TLContains<long, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<char, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int, char, bool, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<char, bool, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int, bool, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int, char, short>, intial_list>);
	STATIC_REQUIRE(TLContains<TL<int, char, bool>, intial_list>);
	STATIC_REQUIRE(!TLContains<TL<int, char, bool, long>, intial_list>);
	STATIC_REQUIRE(!TLContains<float, intial_list>);
}