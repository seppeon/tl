#include "Hz/TL/TLRemoveAll.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveAll", "[TL::TLRemoveAll]")
{
	using namespace Hz;

	using list = TL<int, short, char, int, short, int>;
	using removed_short = TLRemoveAll<list, short>;
	STATIC_REQUIRE(std::is_same_v<removed_short, TL<int, char, int, int>>);
}