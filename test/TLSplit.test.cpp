#include "Hz/TL/TLSplit.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLSplit", "[Hz::TLSplit]")
{
	using namespace Hz;
	using impl = TLSplit<TL<int, char, short, long>, 2>;
	using lhs = typename impl::lhs;
	using rhs = typename impl::rhs;
	STATIC_REQUIRE(std::is_same_v<lhs, TL<int, char>>);
	STATIC_REQUIRE(std::is_same_v<rhs, TL<short, long>>);
}