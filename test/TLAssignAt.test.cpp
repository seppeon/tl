#include "Hz/TL/TLAssignAt.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLAssignAt", "[TL::TLAssignAt]")
{
	using namespace Hz;
	using test_list1 = TL<int, bool, char, short, unsigned>;
	using type_at_0 = TLAssignAt<test_list1, 0u, void>;
	using type_at_1 = TLAssignAt<test_list1, 1u, void>;
	using type_at_2 = TLAssignAt<test_list1, 2u, void>;
	using type_at_3 = TLAssignAt<test_list1, 3u, void>;
	using type_at_4 = TLAssignAt<test_list1, 4u, void>;

	STATIC_REQUIRE(std::is_same_v<type_at_0, TL<void, bool, char, short, unsigned>>);
	STATIC_REQUIRE(std::is_same_v<type_at_1, TL<int, void, char, short, unsigned>>);
	STATIC_REQUIRE(std::is_same_v<type_at_2, TL<int, bool, void, short, unsigned>>);
	STATIC_REQUIRE(std::is_same_v<type_at_3, TL<int, bool, char, void, unsigned>>);
	STATIC_REQUIRE(std::is_same_v<type_at_4, TL<int, bool, char, short, void>>);
}