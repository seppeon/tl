#include "Hz/TL/TLReplace.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLReplace", "[TL::TLReplace]")
{
	using namespace Hz;

	using list = TL<short, int, char, bool, char>;
	using t0 = TLReplace<list, bool, unsigned>;
	using t1 = TLReplace<list, int, int>;
	using t2 = TLReplace<list, short, char>;
	using t3 = TLReplace<list, char, bool>;

	STATIC_REQUIRE(std::is_same_v<list, t1>);
	STATIC_REQUIRE(std::is_same_v<t0, TL<short, int, char, unsigned, char>>);
	STATIC_REQUIRE(std::is_same_v<t2, TL<char, int, char, bool, char>>);
	STATIC_REQUIRE(std::is_same_v<t3, TL<short, int, bool, bool, bool>>);
}