#include "Hz/TL/TLRemoveN.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveN", "[TL::TLRemoveN]")
{
	using namespace Hz;

	using list = TL<int, short, char, int, short, int>;
	using removed_short = TLRemoveN<list, int, 2>;
	STATIC_REQUIRE(std::is_same_v<removed_short, TL<short, char, short, int>>);
}