#include "Hz/TL/TLIndexOf.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLIndexOf", "[TL::TLIndexOf]")
{
	using namespace Hz;

	using list = TL<int, char, short>;
	static constexpr auto int_index = TLIndexOf<list, int>;
	static constexpr auto char_index = TLIndexOf<list, char>;
	static constexpr auto short_index = TLIndexOf<list, short>;
	STATIC_REQUIRE(int_index == 0);
	STATIC_REQUIRE(char_index == 1);
	STATIC_REQUIRE(short_index == 2);
}