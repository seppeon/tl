#include "Hz/TL/TLCountIf.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("CountIf", "[Traits::CountIf]")
{
	using namespace Hz;

	using test_list0 = TL<size_t, bool, bool, size_t, char, bool, short, char>;
	using test_list1 = TL<size_t>;
	using test_list2 = EmptyList;

	static constexpr size_t size_t_count0 	= TLMatchCount<test_list0, size_t>;
	static constexpr size_t bool_count0 	= TLMatchCount<test_list0, bool>;
	static constexpr size_t char_count0 	= TLMatchCount<test_list0, char>;
	static constexpr size_t short_count0 	= TLMatchCount<test_list0, short>;
	static constexpr size_t size_t_match0 	= TLFirstMatch<test_list0, size_t>;
	static constexpr size_t bool_match0 	= TLFirstMatch<test_list0, bool>;
	static constexpr size_t char_match0 	= TLFirstMatch<test_list0, char>;
	static constexpr size_t short_match0 	= TLFirstMatch<test_list0, short>;
	static constexpr size_t size_t_count1 	= TLMatchCount<test_list1, size_t>;
	static constexpr size_t bool_count1 	= TLMatchCount<test_list1, bool>;
	static constexpr size_t size_t_match1 	= TLFirstMatch<test_list1, size_t>;
	static constexpr size_t bool_match1 	= TLFirstMatch<test_list1, bool>;
	static constexpr size_t size_t_count2 	= TLMatchCount<test_list2, size_t>;
	static constexpr size_t bool_count2 	= TLMatchCount<test_list2, bool>;
	static constexpr size_t bool_match2 	= TLFirstMatch<test_list2, bool>;

	STATIC_REQUIRE(size_t_count0 == 2);
	STATIC_REQUIRE(bool_count0 == 3);
	STATIC_REQUIRE(char_count0 == 2);
	STATIC_REQUIRE(short_count0 == 1);
	STATIC_REQUIRE(size_t_match0 == 0);
	STATIC_REQUIRE(bool_match0 == 1);
	STATIC_REQUIRE(char_match0 == 4);
	STATIC_REQUIRE(short_match0 == 6);
	STATIC_REQUIRE(size_t_count1 == 1);
	STATIC_REQUIRE(bool_count1 == 0);
	STATIC_REQUIRE(size_t_match1 == 0);
	STATIC_REQUIRE(bool_match1 >= 1);
	STATIC_REQUIRE(size_t_count2 == 0);
	STATIC_REQUIRE(bool_count2 == 0);
	STATIC_REQUIRE(bool_match2 == 0);
};