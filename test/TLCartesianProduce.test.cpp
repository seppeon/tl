#include "Hz/TL/TLCartesianProduct.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Cartesian product of types", "[TL::TLCartesianProduct]")
{
	using short_expected_result = Hz::TL<
		Hz::TL<int, bool>,
		Hz::TL<short, bool>,
		Hz::TL<int, int>,
		Hz::TL<short, int>
	>;
	STATIC_REQUIRE(std::is_same_v<Hz::TLCartesianProduct<Hz::TL<int, short>, Hz::TL<bool, int>>, short_expected_result>);

	using long_expected_result = Hz::TL<
		Hz::TL<int, bool, long>,
		Hz::TL<short, bool, long>,
		Hz::TL<int, int, long>,
		Hz::TL<short, int, long>,
		Hz::TL<int, bool, float>,
		Hz::TL<short, bool, float>,
		Hz::TL<int, int, float>,
		Hz::TL<short, int, float>
	>;
	STATIC_REQUIRE(std::is_same_v<Hz::TLCartesianProduct<Hz::TL<int, short>, Hz::TL<bool, int>, Hz::TL<long, float>>, long_expected_result>);
}

TEST_CASE("Cartesian product of types are invocable", "[TL::TLInvocableCartesianProduct]")
{
	using empty_result = Hz::TLInvocableCartesianProduct<int (*)(int *, int), Hz::TL<int, short>, Hz::TL<bool, int>>;
	STATIC_REQUIRE(std::is_same_v<empty_result, Hz::TL<>>);

	using pass_result = Hz::TLInvocableCartesianProduct<int (*)(int *, int), Hz::TL<int *, short>, Hz::TL<bool, int>>;
	STATIC_REQUIRE(std::is_same_v<pass_result, Hz::TL<Hz::TL<int *, bool>, Hz::TL<int *, int>>>);
}