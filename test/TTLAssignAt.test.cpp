#include "Hz/TL/TTLAssignAt.hpp"
#include <catch2/catch_all.hpp>
#include <list>
#include <queue>
#include <vector>
#include <tuple>

TEST_CASE("TTLAssignAt", "[TL::TTLAssignAt]")
{
	using list = Hz::TTL<std::tuple, std::vector, std::list>;
	STATIC_REQUIRE(std::same_as<Hz::TTLAssignAt<list, 0, std::queue>, Hz::TTL<std::queue, std::vector, std::list>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAssignAt<list, 1, std::queue>, Hz::TTL<std::tuple, std::queue, std::list>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAssignAt<list, 2, std::queue>, Hz::TTL<std::tuple, std::vector, std::queue>>);
}