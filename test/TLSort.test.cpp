#include "Hz/TL/TLSort.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLSort", "[TL::TLSort]")
{
	using namespace Hz;

	using order_list	= TL<int, short, char, bool>;
	using predicate		= TypeRankPredicate<order_list>;

	using sort_list0	= TL<short, bool, bool, int, short, bool, char, short, int>;
	using sort_list1	= TL<short>;
	using sort_list2	= TL<short, int>;
	using sort_list3	= TL<>;
	using sort_list4	= TL<int, int, short>;
	using sort_list5	= TL<short, int, int>;
	using sort_list6	= TL<int, short, int>;

	using sorted0		= TLSort<sort_list0, predicate>;
	using sorted1		= TLSort<sort_list1, predicate>;
	using sorted2		= TLSort<sort_list2, predicate>;
	using sorted3		= TLSort<sort_list3, predicate>;
	using sorted4		= TLSort<sort_list4, predicate>;
	using sorted5		= TLSort<sort_list5, predicate>;

	using sorted6		= TLSort<sort_list6, predicate>;

	STATIC_REQUIRE(std::is_same_v<sorted0, TL<int, int, short, short, short, char, bool, bool, bool>>);
	STATIC_REQUIRE(std::is_same_v<sorted1, TL<short>>);
	STATIC_REQUIRE(std::is_same_v<sorted2, TL<int, short>>);
}