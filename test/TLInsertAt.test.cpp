#include "Hz/TL/TLInsertAt.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLInsertAt", "[TL::TLInsertAt]")
{
	using namespace Hz;

	using list = TL<int, short>;
	using replaced0 = TLInsertAt<list, 0, long>;
	using replaced1 = TLInsertAt<list, 190009, long>;

	STATIC_REQUIRE(std::is_same_v<replaced0, TL<long, int, short>>);
	STATIC_REQUIRE(std::is_same_v<replaced1, TL<int, short, long>>);
}