#include "Hz/TL/TLTypeArray.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>
#include <variant>

TEST_CASE("TLTypeArray", "[TL::TLTypeArray]")
{
	using namespace Hz;

	using l0 = TLTypeArray<int, 0, TL>;
	using l1 = TLTypeArray<int, 1, std::tuple>;
	using l2 = TLTypeArray<int, 2, std::variant>;
	using l3 = TLTypeArray<int, 30>;

	STATIC_REQUIRE(TLLength<l0> == 0);
	STATIC_REQUIRE(TLLength<l1> == 1);
	STATIC_REQUIRE(TLLength<l2> == 2);
	STATIC_REQUIRE(TLLength<l3> == 30);
}