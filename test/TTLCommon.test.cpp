#include "Hz/TL/TTLCommon.hpp"
#include <catch2/catch_all.hpp>
#include <queue>
#include <vector>
#include <list>
#include <tuple>

TEST_CASE("TTLCommon", "[TL::TTLCommon]")
{
	using list = Hz::TTL<std::tuple, std::vector>;

	STATIC_REQUIRE(std::same_as<Hz::TTLEmptyList, Hz::TTL<>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLEmpty<list>, Hz::TTL<>>);
	STATIC_REQUIRE(std::same_as<Hz::TTypeApply<Hz::TType<std::vector>, int>, std::vector<int>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLFront<list>, Hz::TType<std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLBack<list>, Hz::TType<std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPopFront<list>, Hz::TTL<std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPopBack<list>, Hz::TTL<std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushFront<list, std::list>, Hz::TTL<std::list, std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushBack<list, std::list>, Hz::TTL<std::tuple, std::vector, std::list>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushFrontIf<true, list, std::list>, Hz::TTL<std::list, std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushBackIf<true, list, std::list>, Hz::TTL<std::tuple, std::vector, std::list>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushFrontIf<false, list, std::list>, Hz::TTL<std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushBackIf<false, list, std::list>, Hz::TTL<std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushFrontIfElse<true, list, std::list, std::queue>, Hz::TTL<std::list, std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushBackIfElse<true, list, std::list, std::queue>, Hz::TTL<std::tuple, std::vector, std::list>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushFrontIfElse<false, list, std::list, std::queue>, Hz::TTL<std::queue, std::tuple, std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLPushBackIfElse<false, list, std::list, std::queue>, Hz::TTL<std::tuple, std::vector, std::queue>>);
	STATIC_REQUIRE(Hz::ttl_length_v<list> == 2);
}