#include "Hz/TL/TLReverse.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLReverse", "[TL::TLReverse]")
{
	using namespace Hz;

	using lista_t = TL<int, bool, char>;
	using listb_t = TL<int>;
	using listc_t = TL<>;

	using reversea_t = TLReverse<lista_t>;
	using reverseb_t = TLReverse<listb_t>;
	using reversec_t = TLReverse<listc_t>;

	STATIC_REQUIRE(std::is_same_v<reversea_t, TL<char, bool, int>>);
	STATIC_REQUIRE(std::is_same_v<reverseb_t, TL<int>>);
	STATIC_REQUIRE(std::is_same_v<reversec_t, TL<>>);
}