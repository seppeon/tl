#include "Hz/TL/TLHalve.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLHalve", "[TL::TLHalve]")
{
	using namespace Hz;

	using l0 = TL<int, short, int, char, unsigned>;
	using lhs = typename TLHalve<l0>::lhs;
	using rhs = typename TLHalve<l0>::rhs;
	STATIC_REQUIRE(TLLength<lhs> + TLLength<rhs> == TLLength<l0>);
}