#include "Hz/TL/TLMatchIndexes.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLMatchIndexes", "[TL::TLMatchIndexes]")
{
	using namespace Hz;
	using indexes = TLMatchIndexes<TL<int, char, short, int, long>, SameTypePredicate<int>>;
	STATIC_REQUIRE(std::is_same_v<indexes, std::index_sequence<0, 3>>);
}