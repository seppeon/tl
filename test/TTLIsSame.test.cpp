#include "Hz/TL/TTLIsSame.hpp"
#include <catch2/catch_all.hpp>
#include <vector>
#include <list>

TEST_CASE("ttl_is_same_v", "[TL::TTLIsSame]")
{
	STATIC_REQUIRE_FALSE(Hz::ttl_is_same_v<std::vector, std::list>);
	STATIC_REQUIRE(Hz::ttl_is_same_v<std::vector, std::vector>);
}