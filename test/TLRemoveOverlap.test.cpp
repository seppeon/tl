#include "Hz/TL/TLRemoveOverlap.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveOverlap", "[TL::TLRemoveOverlap]")
{
	using namespace Hz;

	using a = TL<char, bool, short>;
	using b = TL<char, int, bool>;
	using res = TLRemoveOverlap<a, b>;
	using lhs = typename res::lhs;
	using rhs = typename res::rhs;
	STATIC_REQUIRE(std::is_same_v<lhs, TL<short>>);
	STATIC_REQUIRE(std::is_same_v<rhs, TL<int>>);
}