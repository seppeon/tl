#include "Hz/TL/TLAllSame.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLAllSame", "[TL::TLAllSame]")
{
	using namespace Hz;

	STATIC_REQUIRE(TLAllSame<TL<int, int, int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, bool, int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, const int, int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, volatile int, int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, int, const int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, int, volatile int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, int, const int>>);
	STATIC_REQUIRE(!TLAllSame<TL<int, int, volatile int>>);
}