#include "Hz/TL/TLRemoveAt.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLRemoveAt", "[TL::TLRemoveAt]")
{
	using namespace Hz;
	using list = TL<int, short, char>;

	using a = TLRemoveAt<list, 0>;
	using b = TLRemoveAt<list, 1>;
	using c = TLRemoveAt<list, 2>;
	using d = TLRemoveAt<list, 3>;

	STATIC_REQUIRE(std::is_same_v<a, TL<short, char>>);
	STATIC_REQUIRE(std::is_same_v<b, TL<int, char>>);
	STATIC_REQUIRE(std::is_same_v<c, TL<int, short>>);
	STATIC_REQUIRE(std::is_same_v<d, list>);
}
