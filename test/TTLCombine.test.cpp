#include "Hz/TL/TTLCombine.hpp"
#include "Hz/TL/TTLCommon.hpp"
#include <catch2/catch_all.hpp>
#include <tuple>
#include <vector>
#include <list>

TEST_CASE("TTLCombine", "[TL::TTLCombine]")
{
	using list_a = Hz::TTL<std::tuple, std::vector>;
	using list_b = Hz::TTL<std::vector, std::list>;
	using list_ab = Hz::TTLCombine<Hz::TTL, list_a, list_b>; 
	using list_eb = Hz::TTLCombine<Hz::TTL, Hz::TTLEmptyList, list_b>; 
	using list_ae = Hz::TTLCombine<Hz::TTL, list_a, Hz::TTLEmptyList>; 
	using expected_list_ab =  Hz::TTL<std::tuple, std::vector, std::vector, std::list>;
	STATIC_REQUIRE(std::same_as<list_ab, expected_list_ab>);
	STATIC_REQUIRE(std::same_as<list_eb, list_b>);
	STATIC_REQUIRE(std::same_as<list_ae, list_a>);
}