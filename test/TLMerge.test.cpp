#include "Hz/TL/TLMerge.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLMerge", "[TL::TLMerge]")
{
	using namespace Hz;

	using lista_t = TL<int, char, bool, float>;
	using listb_t = TL<int, char, float, double>;
	using listc_t = TL<bool>;

	using merge_t = TLMerge<lista_t, listb_t, listc_t>;
	STATIC_REQUIRE(std::is_same_v<merge_t, TL<int, char, float, double, bool>>);
}