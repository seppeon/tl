#include "Hz/TL/TLCombine.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLCombine", "[TL::TLCombine]")
{
	using namespace Hz;

	using combinebase0		= TL<int, int>;
	using combinetest0		= TL<int, int, int>;
	using combinetest0_1	= TLCombine<TL, combinebase0, int>;
	using combinetest0_2	= TLCombine<TL, int, combinebase0>;
	using combinetest0_3	= TLCombine<TL, int, int, int>;
	using combinetest0_4	= TLCombine<TL, combinetest0>;
	using combinetest0_1_2  = TLCombine<TL, combinebase0, combinetest0, combinetest0_1>;
	using combinebase1		= TL<bool, int>;
	using combinetest1		= TL<short, char, unsigned>;
	using combinetest1_1	= TLCombine<TL, combinebase1, int>;
	using combinetest1_2	= TLCombine<TL, int, combinebase1>;
	using combinetest1_3	= TLCombine<TL, combinebase1, combinetest1>;

	STATIC_REQUIRE(std::is_same_v<combinetest0_1_2, TL<int, int, int, int, int, int, int, int>>);
	STATIC_REQUIRE(std::is_same_v<combinetest0, combinetest0_1>);
	STATIC_REQUIRE(std::is_same_v<combinetest0, combinetest0_2>);
	STATIC_REQUIRE(std::is_same_v<combinetest0, combinetest0_3>);
	STATIC_REQUIRE(std::is_same_v<combinetest0, combinetest0_4>);
	STATIC_REQUIRE(std::is_same_v<TL<bool, int, int>, combinetest1_1>);
	STATIC_REQUIRE(std::is_same_v<TL<int, bool, int>, combinetest1_2>);
	STATIC_REQUIRE(std::is_same_v<TL<bool, int, short, char, unsigned>, combinetest1_3>);
}