#include "Hz/TL/TLFlatten.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLFlatten", "[TL::TLFlatten]")
{
	using namespace Hz;
	using list_a = TL<int, short, double>;
	using list_b = TL<long, list_a>;
	using list_c = TL<list_b, char>;
	STATIC_REQUIRE(std::is_same_v<TLFlatten<TL, list_c>, TL<long, int, short, double, char>>);
}