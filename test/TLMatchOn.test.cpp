#include "Hz/TL/TLMatchOn.hpp"
#include "Hz/TL/TLAtIndex.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLMatchOn", "[TL::TLMatchOn]")
{
	using namespace Hz;

	using intial_list = TL<int, char, bool, short>;

	using m0 = TLAtIndex<intial_list,0>;
	using m1 = TLAtIndex<intial_list,1>;
	using m2 = TLAtIndex<intial_list,2>;
	using m3 = TLAtIndex<intial_list,3>;

	using t0 = TLMatchOn<intial_list, BasicMatchOn<m0>>;
	using t1 = TLMatchOn<intial_list, BasicMatchOn<m1>>;
	using t2 = TLMatchOn<intial_list, BasicMatchOn<m2>>;
	using t3 = TLMatchOn<intial_list, BasicMatchOn<m3>>;

	STATIC_REQUIRE(std::is_same<t0, m0>::value);
	STATIC_REQUIRE(std::is_same<t1, m1>::value);
	STATIC_REQUIRE(std::is_same<t2, m2>::value);
	STATIC_REQUIRE(std::is_same<t3, m3>::value);
}