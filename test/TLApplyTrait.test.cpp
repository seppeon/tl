#include "Hz/TL/TLApplyTrait.hpp"
#include "Hz/TL/TLCommon.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("TLApplyTrait", "[TL::TLApplyTrait]")
{
	using namespace Hz;

	using list = TL<short const, int const volatile>;
	using t0 = TLApplyTrait<list, std::decay>;
	using t1 = TLApplyTrait<list, std::add_pointer>;
	
	STATIC_REQUIRE(std::is_same_v<t0, TL<short, int>>);
	STATIC_REQUIRE(std::is_same_v<t1, TL<short const * , int const volatile *>>);
}