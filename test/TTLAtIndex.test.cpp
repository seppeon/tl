#include "Hz/TL/TTLAtIndex.hpp"
#include <catch2/catch_all.hpp>
#include <tuple>
#include <vector>
#include <list>

TEST_CASE("TTLAtIndex", "[TL::TTLAtIndex]")
{
	using list = Hz::TTL<std::tuple, std::vector, std::list>;
	STATIC_REQUIRE(std::same_as<Hz::TTLAtIndex<list, 0>, Hz::TType<std::tuple>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAtIndex<list, 1>, Hz::TType<std::vector>>);
	STATIC_REQUIRE(std::same_as<Hz::TTLAtIndex<list, 2>, Hz::TType<std::list>>);
}