\mainpage TL

A type list library for creating and manipulating type lists. This library is part of the `Hz` library, and is contained within that namespace.

 - [Code](https://gitlab.com/seppeon/tl)
 - [Doco](https://seppeon.gitlab.io/tl/)

# Installation

To use the library:

1. Clone the repo and open the cloned directory:
```
git clone https://gitlab.com/seppeon/tl.git
cd tl
```

2. Create a `build` directory, and enter that directory:
```
mkdir build
cd build
```

3. Install the dependancies (`catch2`) using `conan`:
```
conan install .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

4. This package is not yet in conan center, so to install it locally (for consumption use with conan) run:
```
conan create .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

5. Open the project which you want to consume this library:
  - Add the `tl/0.1.1` dependancy to your conanfile.py, or conanfile.txt (or whatever version you want).
  - Edit your `CMakeLists.txt` file, and you should be able to find the library:
  ```CMAKE
  find_package(TL REQUIRED)
  add_library(ExampleLibrary)
  target_link_libraries(ExampleLibrary PUBLIC Hz::TL)
  ```
  - Run the conan install command, which will install the dependancy for your project.

See licenses of third parties:
 - \subpage LICENSES