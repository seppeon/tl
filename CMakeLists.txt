cmake_minimum_required(VERSION 3.22)
project(Hz_TL VERSION 0.4.1 LANGUAGES CXX DESCRIPTION "A library that allows viewing of multiple columns of data with a single index.")

option(HZ_TL_TESTS_ENABLED  "Are tests ON or OFF?" OFF)
option(HZ_TL_BUILD_DOC      "Build documentation"  OFF)

if (HZ_TL_TESTS_ENABLED)
	add_subdirectory(test)
endif()
if (HZ_TL_BUILD_DOC)
	add_subdirectory(docs/doxygen)
endif()

include(GNUInstallDirs)
add_library(Hz_TL INTERFACE)
add_library(Hz::TL ALIAS Hz_TL)
target_compile_definitions(Hz_TL INTERFACE HZ_TL_LIB_VERSION="${PROJECT_VERSION}")
target_compile_definitions(Hz_TL INTERFACE HZ_TL_LIB_MAJOR_VERSION=${PROJECT_VERSION_MAJOR})
target_compile_definitions(Hz_TL INTERFACE HZ_TL_LIB_MINOR_VERSION=${PROJECT_VERSION_MINOR})
target_compile_definitions(Hz_TL INTERFACE HZ_TL_LIB_PATCH_VERSION=${PROJECT_VERSION_PATCH})
target_include_directories(Hz_TL
	INTERFACE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

include(cmake/install.cmake)
include(cmake/cpack.cmake)